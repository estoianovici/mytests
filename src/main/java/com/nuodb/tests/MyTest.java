package com.nuodb.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

/**
 * Utility class implementing common test setup methods
 */
public class MyTest {
    private String hostName = "localhost";
    private String databaseName = "test";
    private String schemaName = "user";
    private String userName = "dba";
    private String password = "dba";
    private String port = "48004";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
    private boolean doConnectionSetup = true;

    public void readConfiguration() {
        Properties properties = getProperties();
        hostName = properties.getProperty("nuodb.test.host", hostName);
        databaseName = properties.getProperty("nuodb.test.database", databaseName);
        schemaName = properties.getProperty("nuodb.test.schema", schemaName);
        userName = properties.getProperty("nuodb.test.user", userName);
        password = properties.getProperty("nuodb.test.password", password);
        port = properties.getProperty("nuodb.test.port", port);
    }

    public Properties getProperties() {
        String propertiesFile = System.getProperty("nuodb.test.propertiesFile", "MyTests.properties");
        System.out.println("Current folder: " + System.getProperty("user.dir"));
        File f = new File(propertiesFile);
        if (f.exists() && !f.isDirectory()) {
            try {
                Properties properties = new Properties();
                properties.load(new FileInputStream(f));
                return properties;
            } catch (IOException e) {
                System.out.println("Failed to read properties file: " + e.getMessage());
            }
        }
        System.out.println("Could not load " + propertiesFile);
        System.out.println("Using system properties");
        return System.getProperties();
    }

    public String getHostName() {
        return hostName;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getPort() {
        return port;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public Connection openConnection() throws SQLException {
        Properties properties = new Properties();
        properties.setProperty("user", userName);
        properties.setProperty("password", password);
        properties.setProperty("schema", schemaName);

        String url = String.format("jdbc:com.nuodb://%s:%s/%s", hostName, port, databaseName);
        Connection connection = DriverManager.getConnection(url, properties);
        if (doConnectionSetup()) {
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        }
        return connection;
    }

    protected void runQueries(Connection c, String... queries) throws SQLException {
        try (Statement stmt = c.createStatement()) {
            for (String query : queries) {
                System.out.println(query);
                stmt.execute(query);
            }
        }
    }

    /**
     * Time the execution of a query
     * 
     * @param connection
     *            connection to execute the query on
     * @param query
     *            query to execute
     * @return query execution duration
     * @throws SQLException
     *             if the execution of the query throws
     */
    public static long timeQuery(Connection connection, String query) throws SQLException {
        long start = System.currentTimeMillis();
        try (Statement s = connection.createStatement()) {
            s.execute(query);
        } catch (SQLException e) {
            System.out.println(String.format("failed '%s' : %s", query, e.getMessage()));
            return 0;
        }
        return System.currentTimeMillis() - start;
    }

    public void tryExecute(Connection c, String query) {
        tryExecute(c, query, false);
    }
    public void tryExecute(Connection c, String query, boolean log) {
        if (log) {
            System.out.println(query);
        }
        try (Statement s = c.createStatement()) {
            s.execute(query);
        } catch (SQLException e) {
            if (log) {
                System.out.println("tryExecute:" + e.getMessage());
            }
        }
    }

    public static void executeAndConsume(Statement s, String query) throws SQLException {
        if (s.execute(query)) {
            ResultSet rs = s.getResultSet();
            while (rs.next()) {
                ;
            }
            rs.close();
        }
    }

    protected static void logMaximum(String info, Connection connection, long maximum) {
        String tranId = "";
        try {
            // commit to get a new tID for the log
            connection.commit();
        } catch (Exception e) {
        }
        final String query = "select transid from system.connections where connid = GETCONNECTIONID()";
        try (Statement stmt = connection.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            if (rs.next()) {
                tranId = rs.getString(1);
            }
        } catch (Exception e) {
            // ignore
        }
        System.out.println(String.format("\n[%s] new maximum %d ms for transaction %s", info, maximum, tranId));
    }

    public static String currentTime() {
        return dateFormat.format(Calendar.getInstance().getTime());
    }

    public enum TARGET {
        NUODB, NUODB_OMEGA, POSTGRES, SQLSERVER, DB2, ORACLE, MySQL
    };

    private TARGET target = TARGET.NUODB;

    protected void setTarget(TARGET target) {
        this.target = target;
    }

    protected TARGET getTarget() {
        return target;
    }

    protected Connection connect() throws SQLException {
        switch (target) {
        case NUODB:
            return openConnection();

        case NUODB_OMEGA: {
            Properties properties = new Properties();
            properties.setProperty("user", userName);
            properties.setProperty("password", password);
            properties.setProperty("schema", schemaName);
            properties.setProperty("SQLEngine", "omega");

            String url = String.format("jdbc:com.nuodb://%s:%s/%s", hostName, port, databaseName);
            Connection connection = DriverManager.getConnection(url, properties);
            connection.setAutoCommit(false);
            return connection;
        }

        case POSTGRES: {
            String url = "jdbc:postgresql://localhost:5432/postgres";
            Properties props = new Properties();
            props.setProperty("user", "postgres");
            props.setProperty("password", "admin");
            return DriverManager.getConnection(url, props);
        }

        case SQLSERVER: {
            String url = "jdbc:sqlserver://localhost";
            Properties props = new Properties();
            props.setProperty("databaseName", "master");
            props.setProperty("user", "sa");
            props.setProperty("password", "M1cr@softSucks");
            return DriverManager.getConnection(url, props);
        }

        case DB2: {
            String url = "jdbc:db2://localhost:50000/db2db";
            Properties props = new Properties();
            props.setProperty("databaseName", "db2db");
            props.setProperty("user", "db2inst1");
            props.setProperty("password", "db2inst1-pwd");
            return DriverManager.getConnection(url, props);
        }

        case ORACLE: {
            String url = "jdbc:oracle:thin:@localhost:1521:xe";
            Properties props = new Properties();
            props.setProperty("user", "system");
            props.setProperty("password", "root");
            return DriverManager.getConnection(url, props);
        }
        case MySQL: {
            String url = "jdbc:mysql://127.0.0.1:3306/nuodbtest";
            Properties props = new Properties();
            props.setProperty("user", "root");
            props.setProperty("password", "root");
            props.setProperty("useSSL", "false");
            return DriverManager.getConnection(url, props);
        }
        }
        return null;
    }

    public boolean doConnectionSetup() {
        return doConnectionSetup;
    }

    public void setConnectionSetup(boolean doConnectionSetup) {
        this.doConnectionSetup = doConnectionSetup;
    }
}
