package com.nuodb.tests.workers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.nuodb.tests.DDLScriptTest;
import com.nuodb.tests.MyTest;
import com.nuodb.tests.logging.DurationLogMessage;
import com.nuodb.tests.logging.LogMessage;

public class SQLScriptExecutor {
    private String inputFilePath;
    private String schemaName;
    ConcurrentLinkedQueue<LogMessage<?>> messageQueue;

    public SQLScriptExecutor(String filePath, String schemaName, ConcurrentLinkedQueue<LogMessage<?>> messageQueue) {
        this.inputFilePath = filePath;
        this.schemaName = schemaName;
        this.messageQueue = messageQueue;
    }

    public long timeScriptExecution(Connection connection)
            throws FileNotFoundException, IOException, SQLException {
        long start = System.currentTimeMillis();
        try (Statement s = connection.createStatement();
                BufferedReader reader = new BufferedReader(new FileReader(inputFilePath))) {
            for (String query = reader.readLine(); query != null; query = reader.readLine()) {
                MyTest.executeAndConsume(s, query);
            }
        }
        return System.currentTimeMillis() - start;

    }

    public long executeScript(Connection connection, long maximum)
            throws FileNotFoundException, IOException, SQLException {
        try (Statement s = connection.createStatement()) {
            System.out.println(DDLScriptTest.currentTime() + " DROP SCHEMA");
            s.execute("DROP SCHEMA " + schemaName + " CASCADE");
            System.out.println(DDLScriptTest.currentTime() + " DONE");
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFilePath))) {
            long duration = 0;
            for (String query = reader.readLine(); query != null; query = reader.readLine()) {
                try {
                    duration = MyTest.timeQuery(connection, query);
                    if (duration > maximum) {
                        System.out.println(String.format("%s found new maximum %d for query\n%s",
                                DDLScriptTest.currentTime(), duration, query));
                        maximum = duration;
                    }
                    DurationLogMessage msg = new DurationLogMessage(duration);
                    messageQueue.add(msg);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return maximum;
    }
}