package com.nuodb.tests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class SelectStressTest extends MyTest {
    private volatile boolean keepRunning;
    private boolean doSetup;
    private int numThreads = 10;
    private int duration = 60;
    private int[] threadCounts;

    public SelectStressTest() {
        setTarget(TARGET.NUODB);
    }

    public static void main(String[] args) {
        try {
            SelectStressTest test = new SelectStressTest();
            test.parseOptions(args);
            if (test.doSetup) {
                test.setup();
            }
            test.doTest();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void setup() throws SQLException {
        try (Connection c = connect(); Statement s = c.createStatement()) {
            for (int i = 1; i < 11; i++) {
                s.execute(String.format("create table t%d(i int)", i));
                s.execute(String.format("insert into t%d(i int) select rownum() from system.fields limit 30", i));
            }
        }
    }

    private void doTest() throws InterruptedException {
        printTest();

        keepRunning = true;

        threadCounts = new int[numThreads];
        Thread[] threads = new Thread[numThreads];

        for (int i = 0; i < numThreads; i++) {
            threads[i] = new Thread(new Worker(i));
        }

        for (int i = 0; i < numThreads; i++) {
            threads[i].start();
        }
        
        Thread.sleep(duration * 1000);
        
        keepRunning = false;

        for (int i = 0; i < numThreads; i++) {
            threads[i].join();
        }
        
        long totalQueries = 0;
        for (int i = 0; i < numThreads; i++) {
            totalQueries += threadCounts[i];
        }
        
        System.out.println(String.format("Total: %d, qps: %d", totalQueries, totalQueries/duration));
    }

    private void printTest() {
        System.out.println(String.format("Running for: %d seconds with %d threads", duration, numThreads));
    }

    private void parseOptions(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("setup", "perform setup");
        options.addOption(
                Option.builder("t").longOpt("threads").desc("number of threads").hasArg().argName("threads").build());
        options.addOption(
                Option.builder("d").longOpt("duration").desc("duration to run for in seconds").hasArg().argName("duration").build());

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        if (cmd.hasOption("setup")) {
            doSetup = true;
        } else {
            doSetup = false;
        }

        if (cmd.hasOption("threads")) {
            numThreads = Integer.parseInt(cmd.getOptionValue("threads"));
        } else {
            numThreads = 10;
        }

        if (cmd.hasOption("duration")) {
            duration = Integer.parseInt(cmd.getOptionValue("duration"));
        } else {
            duration = 60;
        }
    }

    class Worker implements Runnable {
        private int id;

        public Worker(int id) {
            this.id = id;
        }

        public void run() {
            int runs = 0;
            try (Connection c = connect(); Statement s = c.createStatement()) {
                while (keepRunning) {
                    ResultSet rs = s.executeQuery("SELECT * FROM DUAL");
                    while (rs.next()) {
                        runs++;
                    }
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            setRun(id, runs);
        }
    }

    private void setRun(int id, int runs) {
        threadCounts[id] = runs;
    }
}
