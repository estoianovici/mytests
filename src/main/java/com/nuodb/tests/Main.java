package com.nuodb.tests;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class Main extends MyTest {

    private static void prepareExecute(Connection c, String query, String param) throws Exception {
        System.out.println(query);
        System.out.println("Using " + param);
        try (PreparedStatement pstmt = c.prepareStatement(query)) {
            pstmt.setByte(1, (byte) 0x1);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                System.out.println(String.format("%s", rs.getInt(1)));
            }
            rs.close();
        }
    }

    public static void executeQuery(Connection c, String query) throws Exception {
        System.out.println(query);
        try (Statement pstmt = c.createStatement()) {
            if (pstmt.execute(query)) {
                ResultSet rs = pstmt.getResultSet();
                while (rs.next()) {
                    System.out.println(String.format("%d %s", rs.getInt(1), rs.getString(2)));
                }
                rs.close();
            }
        }
    }

    public static void main(String[] args) {
        Main m = new Main();
        try (Connection cddl = m.openConnection(); Statement stmt = cddl.createStatement()) {
            stmt.execute("drop table if exists t1");
            stmt.execute("create table t1(i int)");
            cddl.commit();
            while (true) {
                try (Connection c = m.openConnection(); Statement s = c.createStatement()) {
                    c.setAutoCommit(false);
                    c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                    s.execute("LOCK TABLE t1");
                    c.commit();
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

    }
}
