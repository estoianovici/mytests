package com.nuodb.tests;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class DataTypeTest extends MyTest {

	public static void main(String args[]) {
		try {
			DataTypeTest t = new DataTypeTest();
			t.test();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void test() throws Exception {
		try (Connection c = connect(); Statement stmt = c.createStatement()) {
			stmt.execute("drop table if exists t");
			stmt.execute("create table t(n1 numeric(18, 0), n2 numeric(4, 0), n3 numeric(5, 2), n4 numeric(20, 0))");
			stmt.execute("insert into t values(1, 1, 1, 1)");
			ResultSet rs = stmt.executeQuery("select n1, n2, n3, n4, n2 + 1 arith1, 1 + 1 arith2 from t");
			ResultSetMetaData metadata = rs.getMetaData();
			for (int i = 1; i <= metadata.getColumnCount(); i++) {
				System.out.println(String.format("%s: %s", metadata.getColumnName(i), metadata.getColumnTypeName(i)));
			}
			while (rs.next()) {
				for (int i = 1; i <= metadata.getColumnCount(); i++) {
					Object o = rs.getObject(i);
					System.out.println(String.format("%s -> %s", o.getClass().getName(), o.toString()));
				}
			}
		}
	}
}
