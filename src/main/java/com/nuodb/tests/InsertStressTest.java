package com.nuodb.tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class InsertStressTest extends MyTest {
    private volatile boolean keepRunning;
    private boolean doSetup;
    private boolean tableLocks;
    private int numThreads = 10;
    private int duration = 60;
    private int[] threadCounts;

    public InsertStressTest() {
        tableLocks = false;
        doSetup = true;
        setTarget(TARGET.NUODB);
    }

    public static void main(String[] args) {
        try {
            InsertStressTest test = new InsertStressTest();
            test.parseOptions(args);
            test.printTest();
            if (test.doSetup) {
                test.setup();
            }
            test.doTest();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void setup() throws SQLException {
        try (Connection c = connect(); Statement s = c.createStatement()) {
            s.execute(String.format("set system property transactional_locks=%b", tableLocks));
            for (int i = 1; i < 11; i++) {
                s.execute(String.format("drop table if exists t%d", i));
                s.execute(String.format("create table t%d(i int)", i));
            }
        }
    }

    private void doTest() throws InterruptedException {

        keepRunning = true;

        threadCounts = new int[numThreads];
        Thread[] threads = new Thread[numThreads];

        for (int i = 0; i < numThreads; i++) {
            threads[i] = new Thread(new Worker(i));
        }

        for (int i = 0; i < numThreads; i++) {
            threads[i].start();
        }

        Thread.sleep(duration * 1000);

        keepRunning = false;

        for (int i = 0; i < numThreads; i++) {
            threads[i].join();
        }

        long totalQueries = 0;
        for (int i = 0; i < numThreads; i++) {
            totalQueries += threadCounts[i];
        }

        System.out.println(String.format("Total: %d, qps: %d", totalQueries, totalQueries / duration));
    }

    private void printTest() {
        System.out.println(String.format("Running for: %d seconds with %d threads (setup=%b, transactional_locks=%b)",
                duration, numThreads, doSetup, tableLocks));
    }

    private void parseOptions(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption(
                Option.builder("l").longOpt("locks").desc("table locks enabled").hasArg().argName("locks").build());
        options.addOption(
                Option.builder("s").longOpt("setup").desc("perform schema setup").hasArg().argName("setup").build());
        options.addOption(
                Option.builder("t").longOpt("threads").desc("number of threads").hasArg().argName("threads").build());
        options.addOption(
                Option.builder("d").longOpt("duration").desc("duration to run for in seconds").hasArg()
                        .argName("duration").build());

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        if (cmd.hasOption("setup")) {
            doSetup = Boolean.parseBoolean(cmd.getOptionValue("setup"));
        } else {
            doSetup = true;
        }

        if (cmd.hasOption("locks")) {
            tableLocks = Boolean.parseBoolean(cmd.getOptionValue("locks"));
        } else {
            tableLocks = true;
        }

        if (cmd.hasOption("threads")) {
            numThreads = Integer.parseInt(cmd.getOptionValue("threads"));
        } else {
            numThreads = 10;
        }

        if (cmd.hasOption("duration")) {
            duration = Integer.parseInt(cmd.getOptionValue("duration"));
        } else {
            duration = 60;
        }
    }

    class Worker implements Runnable {
        private int id;

        public Worker(int id) {
            this.id = id;
        }

        public void run() {
            int runs = 0;
            String query = String.format("INSERT INTO t%d VALUES(?)", id + 1);
            try (Connection c = connect(); PreparedStatement s = c.prepareStatement(query)) {
                while (keepRunning) {
                    s.setInt(1, runs++);
                    s.execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            setRun(id, runs);
        }
    }

    private void setRun(int id, int runs) {
        threadCounts[id] = runs;
    }
}
