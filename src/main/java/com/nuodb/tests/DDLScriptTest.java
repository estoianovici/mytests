package com.nuodb.tests;

import java.sql.Connection;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.nuodb.tests.logging.FileLogger;
import com.nuodb.tests.logging.LogMessage;
import com.nuodb.tests.workers.SQLScriptExecutor;

public class DDLScriptTest extends MyTest {
	private String inputFileName = "input.sql";
	private String outputFileName = "duration.log";
	private long loggerInterval = 1000; // 1 second

	ConcurrentLinkedQueue<LogMessage<?>> messageQueue;
	private Thread loggerThread;

	public static void main(String args[]) {
		try {
			DDLScriptTest test = new DDLScriptTest();
			test.runTest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DDLScriptTest() {
		readConfiguration();

	}

	@Override
	public void readConfiguration() {
		super.readConfiguration();
		Properties properties = getProperties();
		inputFileName = properties.getProperty("nuodb.test.inputFile", inputFileName);
		outputFileName = properties.getProperty("nuodb.test.outputFile", outputFileName);
		String logging = properties.getProperty("nuodb.test.loggingInterval", "1000");
		loggerInterval = Long.parseLong(logging);
	}

	private void runTest() throws Exception {
		startLoggerThread();
		Thread scriptRunner = new Thread(new Worker());
		scriptRunner.start();
		scriptRunner.join();
		loggerThread.join();
	}

	private void startLoggerThread() {
		messageQueue = new ConcurrentLinkedQueue<>();

		loggerThread = new Thread(new FileLogger(loggerInterval, messageQueue, "duration.log"));
		loggerThread.start();
	}

	public String getInputFileName() {
		return inputFileName;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public long getLoggerInterval() {
		return loggerInterval;
	}

	public ConcurrentLinkedQueue<LogMessage<?>> getMessageQueue() {
		return messageQueue;
	}

	private class Worker implements Runnable {

		@Override
		public void run() {
			try (Connection c = openConnection()) {
				c.setAutoCommit(true);
				SQLScriptExecutor executor = new SQLScriptExecutor(getInputFileName(), getSchemaName(),
						getMessageQueue());
				long max = 0;
				while (true) {
					long duration = executor.executeScript(c, max);
					if (duration > max)
						max = duration;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
