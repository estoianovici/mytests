package com.nuodb.tests;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Random;

public class Truncate extends MyTest {
	private static final int permanentTablesCount = 500;
	private boolean bestNode = false;

	public static void main(String args[]) {
		Truncate test = new Truncate();
		try {
			test.runTest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void runTest() throws Exception {
		System.out.println("Setting up initial schema");
		setup();
		System.out.println("Done");
		doTruncate();
	}

	/**
	 * Setup test
	 * 
	 * this method cleans the test schema and creates 500 initial tables
	 * 
	 * @throws Exception
	 */
	private void setup() throws Exception {
		readConfiguration();
		try (Connection c = openConnection(); Statement s = c.createStatement()) {
			s.execute("DROP SCHEMA " + getSchemaName() + " CASCADE");
			for (int i = 0; i < permanentTablesCount; i++) {
				s.execute(String.format("CREATE TABLE a%d(a%d_i int, a%d_s string)", i, i, i));
			}
			c.commit();
		}
	}

	private void doTruncate() throws Exception {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter("truncate_durations.log", true));
				Connection c = openNonChairmanConnection();
				Statement s = c.createStatement()) {
			long max = 0;
			Random r = new Random(System.currentTimeMillis());
			while (true) {
				int table = 1 + r.nextInt(permanentTablesCount - 2);
				long duration = timeQuery(c, String.format("TRUNCATE TABLE a%d", table));
				writer.write(String.format("%s %d\n", MyTest.currentTime(), duration));
				writer.flush();
				if (duration > max) {
					max = duration;
					logMaximum("truncate", c, max);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Connection openNonChairmanConnection() throws Exception {
		Connection c;
		final String query = bestNode
				? "select nodeid from system.connections where connid = getconnectionid() and nodeid in (select min(id) + 1 from system.nodes)"
				: "select nodeid from system.connections where connid = getconnectionid() and nodeid not in (select min(id) + 1 from system.nodes)";
		while (true) {
			c = openConnection();
			try (Statement s = c.createStatement(); ResultSet rs = s.executeQuery(query)) {
				if (rs.next()) {
					System.out.println("Connected to node " + rs.getInt(1));
					break;
				}
			}
		}
		c.setAutoCommit(true);
		return c;
	}

	@Override
	public void readConfiguration() {
		super.readConfiguration();
		Properties properties = getProperties();
		String bestNodeStr = properties.getProperty("nuodb.test.useBestNode", "false");
		if (Boolean.getBoolean(bestNodeStr)) {
			bestNode = true;
		}
	}
}
