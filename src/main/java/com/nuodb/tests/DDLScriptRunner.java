package com.nuodb.tests;

import java.sql.Connection;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.nuodb.tests.logging.ConsoleLogger;
import com.nuodb.tests.workers.SQLScriptExecutor;

/**
 * Executes a SQL Script using a JDBC connection
 */
public class DDLScriptRunner extends DDLScriptTest {

	public static void main(String args[]) {
		DDLScriptRunner runner = new DDLScriptRunner();
		runner.runTest();
	}

	private void runTest() {
		readConfiguration();
		messageQueue = new ConcurrentLinkedQueue<>();
		Thread loggerThread = new Thread(new ConsoleLogger(getLoggerInterval(), getMessageQueue()));
		loggerThread.start();
		try (Connection c = openConnection()) {
			c.setAutoCommit(true);
			long maximum = 0;
			SQLScriptExecutor executor = new SQLScriptExecutor(getInputFileName(), getSchemaName(), getMessageQueue());
			maximum = executor.executeScript(c, maximum);
			loggerThread.join(2000);
		} catch (InterruptedException e) {
			System.out.println("logger killed: " + e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
