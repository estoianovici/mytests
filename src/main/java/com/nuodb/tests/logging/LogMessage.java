package com.nuodb.tests.logging;

public interface LogMessage<T> extends Comparable<T> {
	/**
	 * Get the String representation of the message included in this log message
	 * 
	 * @return log message
	 */
	public String getMessage();
}
