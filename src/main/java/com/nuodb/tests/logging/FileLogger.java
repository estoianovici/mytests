package com.nuodb.tests.logging;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class FileLogger extends Logger {
	private String fileName;

	public FileLogger(long loggingInterval, ConcurrentLinkedQueue<LogMessage<?>> messageQueue, String fileName) {
		super(loggingInterval, messageQueue);
		this.fileName = fileName;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(getLoggingInterval());
			} catch (InterruptedException e) {
				// exit logging
				break;
			}
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))) {
				@SuppressWarnings("rawtypes")
				List<LogMessage> currentMessages = new ArrayList<>();

				while (!getMessageQueue().isEmpty()) {
					currentMessages.add(getMessageQueue().poll());
				}

				if (currentMessages.isEmpty()) {
					System.out.println("no messages");
					continue;
				}

				@SuppressWarnings("unchecked")
				LogMessage<?> logMessage = Collections.max(currentMessages);
				String message = String.format("%s\n", logMessage.getMessage());
				writer.write(message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}