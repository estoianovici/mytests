package com.nuodb.tests.logging;

import java.util.concurrent.ConcurrentLinkedQueue;

/***
 * class that gathers log messages and logs a summary of the activity once per a
 * configured amount of time
 */
public abstract class Logger implements Runnable {
	public Logger(long loggingInterval, ConcurrentLinkedQueue<LogMessage<?>> messageQueue) {
		this.loggingInterval = loggingInterval;
		this.messageQueue = messageQueue;
	}

	private long loggingInterval;
	private ConcurrentLinkedQueue<LogMessage<?>> messageQueue;

	public long getLoggingInterval() {
		return loggingInterval;
	}

	public ConcurrentLinkedQueue<LogMessage<?>> getMessageQueue() {
		return messageQueue;
	}

}