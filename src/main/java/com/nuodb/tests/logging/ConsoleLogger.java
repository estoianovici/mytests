package com.nuodb.tests.logging;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ConsoleLogger extends Logger {

	public ConsoleLogger(long loggingInterval, ConcurrentLinkedQueue<LogMessage<?>> messageQueue) {
		super(loggingInterval, messageQueue);
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(getLoggingInterval());
			} catch (InterruptedException e) {
				// exit logging
				break;
			}
			@SuppressWarnings("rawtypes")
			List<LogMessage> currentMessages = new ArrayList<>();

			while (!getMessageQueue().isEmpty()) {
				currentMessages.add(getMessageQueue().poll());
			}

			if (currentMessages.isEmpty()) {
				System.out.println("no messages");
				continue;
			}

			for (LogMessage<?> logMessage : currentMessages) {
				String message = String.format("%s\n", logMessage.getMessage());
				System.out.println(message);
			}
		}
	}

}
