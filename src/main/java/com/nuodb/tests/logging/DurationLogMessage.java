package com.nuodb.tests.logging;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DurationLogMessage implements LogMessage<DurationLogMessage> {
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
	Long duration;

	public DurationLogMessage(long duration) {
		this.duration = new Long(duration);
	}

	@Override
	public int compareTo(DurationLogMessage other) {
		return this.duration.compareTo(other.duration);
	}

	@Override
	public String getMessage() {
		return String.format("%s %d", dateFormat.format(Calendar.getInstance().getTime()), duration.longValue());
	}

}