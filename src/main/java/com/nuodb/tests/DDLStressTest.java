package com.nuodb.tests;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;

public class DDLStressTest extends MyTest {
	int permanentTablesCount = 500;

	public static void main(String args[]) {
		long start = System.currentTimeMillis();
		DDLStressTest test = new DDLStressTest();
		try {
			test.runTest();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Completed in " + (System.currentTimeMillis() - start) + " ms");
	}

	private void runTest() throws Exception {
		System.out.println("Setting up initial schema");
		setup();
		System.out.println("Done");
		Thread dropCreate = new Thread(new DropCreate());
		dropCreate.start();
		Thread.sleep(1000);
		Thread truncate = new Thread(new Truncate());
		truncate.start();

		dropCreate.join();
		truncate.join();
	}

	/**
	 * Setup test
	 * 
	 * this method cleans the test schema and creates 500 initial tables
	 * 
	 * @throws Exception
	 */
	private void setup() throws Exception {
		readConfiguration();
		try (Connection c = openConnection(); Statement s = c.createStatement()) {
			s.execute("DROP SCHEMA " + getSchemaName() + " CASCADE");
			for (int i = 0; i < permanentTablesCount; i++) {
				s.execute(String.format("CREATE TABLE a%d(a%d_i int, a%d_s string)", i, i, i));
			}
		}
	}

	private class DropCreate implements Runnable {
		@Override
		public void run() {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			try (BufferedWriter writer = new BufferedWriter(new FileWriter("durations.log", true));
					Connection c = openConnection();
					Statement s = c.createStatement()) {
				s.execute("DROP SCHEMA " + getSchemaName() + " CASCADE");
				long max = 0;
				int i = permanentTablesCount + 1;
				while (true) {
					long duration = timeQuery(c, String.format("CREATE TABLE a%d(a%d_i int, a%d_s string)", i, i, i));
					writer.write(
							String.format("%s %d\n", dateFormat.format(Calendar.getInstance().getTime()), duration));
					writer.flush();
					if (duration > max) {
						max = duration;
						logMaximum("create", c, max);
					}
					s.execute(String.format("DROP TABLE a%d", i));

					if (i > 40000) {
						// reset count
						i = permanentTablesCount;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class Truncate implements Runnable {
		@Override
		public void run() {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			try (BufferedWriter writer = new BufferedWriter(new FileWriter("truncate_durations.log", true));
					Connection c = openConnection();
					Statement s = c.createStatement()) {
				long max = 0;
				Random r = new Random(System.currentTimeMillis());
				while (true) {
					int table = r.nextInt(permanentTablesCount);
					long duration = timeQuery(c, String.format("TRUNCATE TABLE a%d", table));
					writer.write(
							String.format("%s %d\n", dateFormat.format(Calendar.getInstance().getTime()), duration));
					writer.flush();
					if (duration > max) {
						max = duration;
						logMaximum("truncate", c, max);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public Connection openConnection() throws SQLException {
		Connection c = super.openConnection();
		c.setAutoCommit(true);
		return c;
	}
}
