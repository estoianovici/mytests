package com.nuodb.tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class RCInsertDeleteTest extends MyTest {

	static volatile boolean keepRunning = true;
	private String[] getDDL() {
		switch (getTarget()) {
		case NUODB: {
			String[] statements = { "DROP TABLE t IF EXISTS",
					"CREATE TABLE t (f1 INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, f2 INTEGER, f3 INTEGER)" };
			return statements;
		}
		case POSTGRES: {
			String[] statements = { "DROP TABLE IF EXISTS t",
					"CREATE TABLE t (f1 SERIAL, f2 INTEGER, f3 INTEGER , PRIMARY KEY(f1))" };
			return statements;
		}
		}
		return null;
	}

	private void doDDLSetup() throws Exception {
		try (Connection c = connect(); Statement stmt = c.createStatement()) {
			for (String s : getDDL()) {
				stmt.execute(s);
			}
		}
	}

	public static void main(String args[]) {
		try {
			RCInsertDeleteTest test = new RCInsertDeleteTest();
			test.doDDLSetup();
			test.runTest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void runTest() throws Exception {
		keepRunning = true;
		Thread threads[] = new Thread[21];
		for (int i = 0; i < 19; i++) {
			threads[i] = new Thread(new Inserter(connect()));
		}
		threads[19] = new Thread(new Deleter(connect()));
		threads[20] = new Thread(new Selector(connect()));
		for (int i = 20; i >= 0; i--) {
			threads[i].start();
		}

		for (int i = 20; i >= 0; i--) {
			threads[i].join();
		}
	}

	class Selector implements Runnable {
		private Connection c;

		public Selector(Connection c) throws Exception {
			this.c = c;
			c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			c.setAutoCommit(true);
		}

		@Override
		public void run() {
			while (keepRunning) {
				try (Statement stmt = c.createStatement(); ResultSet rs = stmt.executeQuery("select count(*) from t")) {
					if (rs.next()) {
						long result = rs.getLong(1);
						// System.out.println("Selected " + result + " rows ");
						if (result % 10 != 0) {
							keepRunning = false;
							System.out.println("Found " + result + " rows in the table!");
							rs.close();
							c.commit();
							break;
						}
					}
					Thread.sleep(200);
				} catch (SQLException | InterruptedException e) {
					System.out.println("Deleter: " + e.getMessage());
				}
			}

			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	class Deleter implements Runnable {
		private Connection c;

		public Deleter(Connection c) throws Exception {
			this.c = c;
			c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			c.setAutoCommit(false);
		}

		@Override
		public void run() {
			while (keepRunning) {
				try (Statement stmt = c.createStatement()) {
					stmt.execute("delete from t");
					long result = stmt.getUpdateCount();
					System.out.println("Deleted " + result + " rows ");
					if (result % 10 != 0) {
						keepRunning = false;
						System.out.println("Found " + result + " rows in the table!");
						break;
					}
					c.commit();
					Thread.sleep(250);
				} catch (SQLException | InterruptedException e) {
					System.out.println("Deleter: " + e.getMessage());
				}
			}
			try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	class Inserter implements Runnable {
		private Connection c;
		private Random r;

		public Inserter(Connection c) throws Exception {
			this.c = c;
			c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			c.setAutoCommit(false);
			r = new Random(System.currentTimeMillis());
		}

		@Override
		public void run() {
			while (keepRunning) {
				try (PreparedStatement stmt = c.prepareStatement("insert into t(f2, f3) values(?, ?)")) {
					for (int i = 0; i < 10; i++) {
						stmt.setInt(1, r.nextInt());
						stmt.setInt(2, r.nextInt());
						stmt.execute();
					}
					c.commit();
					Thread.sleep(10);
				} catch (SQLException e) {
					System.out.println("Deleter: " + e.getMessage());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
