import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Properties;
import java.util.TimeZone;

public class TestTimeZones {
    private static final String hostName = "localhost";
    private static final String databaseName = "test";
    private static final String schemaName = "tests";
    private static final String userName = "dba";
    private static final String password = "dba";
    private static final String port = "48004";

    public static void main(String args[]) {
        TestTimeZones test = new TestTimeZones();
        try {
            // test.testTimezones();
            test.testMyTimezone();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void testMyTimezone() throws Exception {
        try (Connection c = connect(); Statement stmt = c.createStatement()) { 
            ResultSet rs = stmt.executeQuery("SELECT property, value FROM system.connectionproperties");
            while (rs.next()) {
                System.out.println(rs.getString(1) + " = " + rs.getString(2));
            }
        }
    }

    public void testTimezones() {
        try (Connection c = connect()) {
            runQueries(c, "drop table if exists t",
                    "create table t(id bigint generated always as identity, timestamp_col timestamp, timezone string)");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Properties properties = new Properties();
        properties.setProperty("user", userName);
        properties.setProperty("password", password);
        properties.setProperty("schema", schemaName);

        testTimeZone(properties, "EST");
        testTimeZone(properties, "UTC");
    }

    private void testTimeZone(Properties properties, String timezone) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
        String url = String.format("jdbc:com.nuodb://%s:%s/%s?TimeZone=%s", hostName, port, databaseName,
                timezone);
        try (Connection c = DriverManager.getConnection(url, properties); Statement stmt = c.createStatement()) {
            logConnectionTimezone(stmt);

            stmt.execute("insert into t(timestamp_col, timezone) "
                    + "values('2010-01-01 00:00:00', (select property||'='||value from system.connectionproperties where property = 'timezone' limit 1))");

            ResultSet rs = stmt.executeQuery("SELECT id, timezone, timestamp_col FROM t");
            while (rs.next()) {
                String line = String.format("%s(%s):{getString= %s, getTimestamp = %s, getTimestampWithTimezone = %s}",
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getTimestamp(3).toString(),
                        rs.getTimestamp(3, cal).toString());
                System.out.println(line);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void logConnectionTimezone(Statement stmt) throws SQLException {
        ResultSet rs = stmt.executeQuery(
                "select property, value from system.connectionproperties where property = 'timezone'");
        while (rs.next()) {
            System.out.println("Connection timezone is:" + rs.getString(2));
        }
    }

    private void runQueries(Connection c, String... queries) throws Exception {
        try (Statement stmt = c.createStatement()) {
            for (String query : queries) {
                System.out.println(query);
                stmt.execute(query);
            }
        }
    }

    private Connection connect() throws SQLException {
        Properties properties = new Properties();
        properties.setProperty("user", userName);
        properties.setProperty("password", password);
        properties.setProperty("schema", schemaName);

        String url = String.format("jdbc:com.nuodb://%s:%s/%s", hostName, port, databaseName);
        return DriverManager.getConnection(url, properties);
    }
}
