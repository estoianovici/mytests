package com.nuodb.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class BatchInsertTest extends MyTest {

    private void setup() {
        try (Connection conn = connect(); Statement statement = conn.createStatement()) {
            statement.execute("drop table if exists user.t");
            statement.execute("create table user.t(i int, s string)");
            conn.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() {
        setTarget(TARGET.NUODB);
        setup();
        int count = 0;
        try (Connection conn = connect(); Statement statement = conn.createStatement()) {
            while (true) {
                for (int i = 0; i < 10; i++) {
                    statement.addBatch("insert into t values(1, 1)");
                }
                int[] counts = statement.executeBatch();
                for (int c : counts) {
                    if (c > 0) {
                        count += c;
                    } else if (c == Statement.SUCCESS_NO_INFO) {
                        count++;
                    } else {
                        System.out.println(String.format("Failure in Batch Execute: %d", c));
                    }
                }

                System.out.println(String.format("Execute Batch: %d rows committed", count));

                ResultSet confirm = statement.executeQuery("select count(*) from user.t;");
                if (confirm.next()) {
                    System.out.println(String.format("%d committed - total in DB: %d", count, confirm.getLong(1)));
                }

                conn.commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
