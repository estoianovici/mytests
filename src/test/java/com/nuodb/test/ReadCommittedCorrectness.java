package com.nuodb.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class ReadCommittedCorrectness extends MyTest {

	/**
	 * Test that insert and delete don't conflict under read committed
	 */
	@Test
	public void testInsertDelete() throws Exception {
		try (Connection c1 = connect()) {
			executeQueries(c1, "drop table if exists t", "create table t(i int, j int)");
			c1.commit();
			// We leak c2 if the test fails. This is unfortunate but if the
			// background task fails, there is no way of knowing the state
			// of c2 and the call to c2.close() will block forever
			Connection c2 = connect();
			executeQueries(c1, "insert into t values(1, 1)", "insert into t values(2, 2)");

			// c2 delete should not be blocked by c1
			Thread deleter = new Thread(new BackgroundExecutor(() -> {
				try (Statement s = c2.createStatement()) {
					s.execute("delete from t");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
			deleter.start();

			// Give the deleter a chance to finish. If we're blocking it, we're
			// not behaving correctly under read committed!
			while (deleter.isAlive()) {
				assertFalse(isBlockingOther(c1));
				deleter.join(1);
			}
			c2.close();
		}
	}

	/**
	 * Test that insert and delete don't conflict under read committed
	 */
	@Test
	public void testInsertUpdate() throws Exception {
		try (Connection c1 = connect()) {
			executeQueries(c1, "drop table if exists t", "create table t(i int, j int)",
					"insert into t values(1, 1), (2, 2)");
			c1.commit();
			// We leak c2 if the test fails. This is unfortunate but if the
			// background task fails, there is no way of knowing the state
			// call to c2.close() will block forever
			Connection c2 = connect();

			executeQueries(c1, "insert into t values(3, 3)", "insert into t values(4, 4)");

			// c2 delete should not be blocked by c1
			Thread updater = new Thread(new BackgroundExecutor(() -> {
				try (Statement s = c2.createStatement()) {
					s.execute("update t set i = 100 where i > 0");
					assertEquals(s.getUpdateCount(), 2);
					ResultSet rs = s.executeQuery("select count(*) from t where i = 100 and j < 3");
					assertTrue(rs.next());
					assertEquals(2, rs.getInt(1));
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
			updater.start();

			// Give the updater a chance to finish. If we're blocking it, we're
			// not behaving correctly under read committed!
			while (updater.isAlive()) {
				assertFalse(isBlockingOther(c1));
				updater.join(1);
			}
			c2.close();
		}
	}

	/**
	 * Test that update/delete don't conflict under read committed
	 */
	@Test
	public void testUpdateDeleteNoConflict() throws Exception {
		try (Connection c1 = connect()) {
			executeQueries(c1, "drop table if exists t", "create table t(i int, j int)",
					"insert into t values(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)");
			c1.commit();
			// We leak c2 if the test fails. This is unfortunate but if the
			// background task fails, there is no way of knowing the state
			// call to c2.close() will block forever
			Connection c2 = connect();

			executeQueries(c1, "update t set j = 100 where i < 4");

			// c2 delete should not be blocked by c1
			Thread deleter = new Thread(new BackgroundExecutor(() -> {
				try (Statement s = c2.createStatement()) {
					s.execute("delete from t where i >= 4");
					assertEquals(s.getUpdateCount(), 3);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
			deleter.start();

			// Give the updater a chance to finish. If we're blocking it, we're
			// not behaving correctly under read committed!
			while (deleter.isAlive()) {
				assertFalse(isBlockingOther(c1));
				deleter.join(1);
			}
			c2.close();
		}
	}

	/**
	 * Test that delete/update don't conflict under read committed
	 */
	@Test
	public void testDeleteUpdateNoConflict() throws Exception {
		try (Connection c1 = connect()) {
			executeQueries(c1, "drop table if exists t", "create table t(i int, j int)",
					"insert into t values(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)");
			c1.commit();
			// We leak c2 if the test fails. This is unfortunate but if the
			// background task fails, there is no way of knowing the state
			// call to c2.close() will block forever
			Connection c2 = connect();

			executeQueries(c1, "delete from t where i < 4");

			// c2 delete should not be blocked by c1
			Thread deleter = new Thread(new BackgroundExecutor(() -> {
				try (Statement s = c2.createStatement()) {
					s.execute("update t set j = 100 where i >= 4");
					assertEquals(s.getUpdateCount(), 3);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
			deleter.start();

			// Give the updater a chance to finish. If we're blocking it, we're
			// not behaving correctly under read committed!
			while (deleter.isAlive()) {
				assertFalse(isBlockingOther(c1));
				deleter.join(1);
			}
			c2.close();
		}
	}

	/**
	 * Test that update/delete conflict on same record under read committed  
	 */
	@Test
	public void testUpdateDeleteConflict() throws Exception {
		try (Connection c1 = connect()) {
			executeQueries(c1, "drop table if exists t", "create table t(i int, j int)",
					"insert into t values(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)");
			c1.commit();
			// We leak c2 if the test fails. This is unfortunate but if the
			// background task fails, there is no way of knowing the state
			// call to c2.close() will block forever
			Connection c2 = connect();

			executeQueries(c1, "update t set j = 100 where i > 4");

			// c2 delete should not be blocked by c1
			Thread deleter = new Thread(new BackgroundExecutor(() -> {
				try (Statement s = c2.createStatement()) {
					s.execute("delete from t where i > 4");
					assertEquals(s.getUpdateCount(), 2);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}));
			deleter.start();

			// Give the updater a chance to finish. If we're blocking it, we're
			// not behaving correctly under read committed!
			while (deleter.isAlive()) {
				if(isBlockingOther(c1)) {
					break;
				}
				deleter.join(1);
			}
			assertTrue(isBlockingOther(c1));
			assertTrue(deleter.isAlive());
			
			c1.commit();
			// deleter should be unblocked now
			
			deleter.join(1000);
			assertFalse(deleter.isAlive());
			c2.close();
		}
	}

	public static void main(String args[]) {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean isBlockingOther(Connection c) throws Exception {
		final String BLOCKED_QUERY = "select id from system.localtransactions where blockedby = gettransactionid()";
		try (Statement s = c.createStatement(); ResultSet r = s.executeQuery(BLOCKED_QUERY)) {
			if (r.next()) {
				// we are blocking at least one connection
				return true;
			}
		}
		return false;
	}

	private void executeQueries(Connection c, String... queries) throws SQLException {
		try (Statement s = c.createStatement()) {
			for (String query : queries) {
				s.execute(query);
			}
		}
	}

	@FunctionalInterface
	interface TransactionAction {
		void doAction();
	}

	class BackgroundExecutor implements Runnable {
		private TransactionAction action;

		public BackgroundExecutor(TransactionAction action) {
			this.action = action;
		}

		@Override
		public void run() {
			action.doAction();
		}

	}
}
