package com.nuodb.test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class TZTest {

    public static void main(String args[]) {
        TimeZone tz = TimeZone.getTimeZone("Europe/Istanbul");
        TimeZone.setDefault(tz);
        GregorianCalendar gc = new GregorianCalendar(1943, 00, 01, 12, 01, 01);
        gc.add(Calendar.MILLISECOND, 123);
        System.out.println(System.getProperty("java.version") +  gc.getTimeInMillis());
    }
}
