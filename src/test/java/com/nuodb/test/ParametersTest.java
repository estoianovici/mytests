package com.nuodb.test;

import com.nuodb.tests.MyTest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import org.junit.Test;

public class ParametersTest extends MyTest {

    private void test() throws Exception {
        System.out.println(getTarget());
        String selectQuery = "SELECT i + ? FROM t WHERE i < ?";
        try (Connection c = connect()) {
            // setup
            tryExecute(c, "drop table t");
            tryExecute(c, "create table t(i int, s varchar(64), d decimal(12, 3))");

            // test parameterized insert
            try (PreparedStatement insert = c.prepareStatement("INSERT INTO t (i) VALUES(?)")) {
                insert.setString(1, "1");
                insert.execute();
            }

            // test parameterized select 
            System.out.println(selectQuery);
            System.out.println("use 1.1");
            try (PreparedStatement select = c.prepareStatement(selectQuery)) {
                ResultSetMetaData metaData = select.getMetaData();
                System.out.println(metaData.getColumnName(1) + ":" + metaData.getColumnClassName(1) + ":" + metaData.getColumnTypeName(1));
                select.setString(1, "1.1");
                select.setString(2, "1.1");
                ResultSet rs = select.executeQuery();
                while (rs.next()) {
                    System.out.println(rs.getString(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("===============================");
    }

    @Test
    public void testDb2() throws Exception {
        setTarget(TARGET.DB2);
        test();
    }

    @Test
    public void testSqlServer() throws Exception {
        setTarget(TARGET.SQLSERVER);
        test();
    }

    @Test
    public void testPostgres() throws Exception {
        setTarget(TARGET.POSTGRES);
        test();
    }

    @Test
    public void testMySQL() throws Exception {
        setTarget(TARGET.MySQL);
        test();
    }

    @Test
    public void testNuoDb() throws Exception {
        setTarget(TARGET.NUODB);
        test();
    }

    @Test
    public void testOracle() throws Exception {
        setTarget(TARGET.ORACLE);
        test();
    }
}
