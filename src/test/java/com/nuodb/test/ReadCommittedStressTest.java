package com.nuodb.test;

import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class ReadCommittedStressTest extends MyTest {
	// @formatter:off
	private final String[] pgSetupQueries = {
			"DROP TABLE IF EXISTS ten",
			"CREATE TABLE ten (f1 INTEGER)",
			"INSERT INTO ten VALUES (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)",
			"DROP TABLE IF EXISTS t1",
			"CREATE TABLE t1 (f2 INTEGER DEFAULT pg_backend_pid(), f3 INTEGER DEFAULT 0)",
			"CREATE INDEX i_t1_f3 on t1(f3)"
	};

	private final String[] nuoSetupQueries = {
			"DROP TABLE IF EXISTS ten",
			"CREATE TABLE ten (f1 INTEGER)",
			"INSERT INTO ten VALUES (0), (0), (0), (0), (0), (0), (0), (0), (0), (0)",
			"COMMIT",
			"DROP TABLE IF EXISTS t1",
			"CREATE TABLE t1 (f1 bigint, f2 BIGINT DEFAULT GETTRANSACTIONID(), f3 BIGINT DEFAULT 0)",
	};
	
	private final String[] pgQueries = {
			"INSERT INTO t1(f3) VALUES (0), (1), (2), (3), (4), (5), (6), (7), (8), (9)",
			"DELETE FROM t1 WHERE f2 = pg_backend_pid()",
			"UPDATE t1 SET f3 = f3 + 1 WHERE f2 = pg_backend_pid()",
			"UPDATE t1 SET f3 = f3 + 1"
	};
	private final String[] nuoQueries = {
			"INSERT INTO t1 (f3) SELECT GETCONNECTIONID() FROM ten",
			"INSERT INTO t1 (f3) SELECT GETCONNECTIONID() FROM ten",
			"INSERT INTO t1 (f3) SELECT GETCONNECTIONID() FROM ten",
			"DELETE FROM t1 ORDER BY f3 ASC LIMIT 10",
	};
	
	private final int[] queryCounts = new int[Math.max(nuoQueries.length, pgQueries.length)];
	private final int[] zeroCounts = new int[Math.max(nuoQueries.length, pgQueries.length)];
	// @formatter:on

	private static volatile boolean keepRunning = true;
	private static volatile int deadlocks = 0;
	private static volatile Random r = new Random(System.currentTimeMillis());
	private static volatile AtomicInteger counter = new AtomicInteger(0);

	@Test
	public void testRC() {
		try {
			testNuoDB();
			// testPgSQL();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void testNuoDB() throws Exception {
		setTarget(TARGET.NUODB);
		test();
	}

	private void testPgSQL() throws Exception {
		setTarget(TARGET.POSTGRES);
		test();
	}

	private void test() throws Exception {
		setup();
		int threadCount = 30;
		keepRunning = true;
		java.util.Arrays.fill(queryCounts, 0);
		java.util.Arrays.fill(zeroCounts, 0);
		Thread[] threads = new Thread[threadCount];
		for (int i = 0; i < threadCount; i++) {
			threads[i] = new Thread(new BackgroundExecutor());
			threads[i].start();
		}

		Thread.sleep(60 * 1000);

		keepRunning = false;
		System.out.println("Done");
		for (int i = 0; i < threadCount; i++) {
			threads[i].join();
		}

		String queries[] = getQueries(getTarget());
		for (int i = 0; i < queries.length; i++) {
			System.out.println(String.format("%s:%s was ran %d (zero rows %d) times", getTarget().toString(), queries[i],
					queryCounts[i], zeroCounts[i]));
		}
		System.out.println(String.format("%s: deadlocked %d times", getTarget().toString(), deadlocks));
	}

	class TestResult {

	}

	private void setup() throws Exception {
		try (Connection c = connect(); Statement s = c.createStatement()) {
			for (String query : getSetupQueries(getTarget())) {
				s.execute(query);
			}
		}
	}

	private String[] getSetupQueries(TARGET target) {
		switch (target) {
		case NUODB:
			return nuoSetupQueries;
		case POSTGRES:
			return pgSetupQueries;
		}
		return new String[0];
	}

	private String[] getQueries(TARGET target) {
		switch (target) {
		case NUODB:
			return nuoQueries;
		case POSTGRES:
			return pgQueries;
		}
		return new String[0];
	}

	class BackgroundExecutor implements Runnable {

		@Override
		public void run() {
			try (Connection c = connect()) {
				c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
				c.setAutoCommit(true);
				while (keepRunning) {
					while (keepRunning) {
						int queryId = r.nextInt(getQueries(getTarget()).length);
						String query = getQueries(getTarget())[queryId];
						testQuery(c, query, queryId);
						queryCounts[queryId]++;
					}
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}

		private void testQuery(Connection c, String query, int queryId) throws Exception {
			String rows = "";
			int updatedCount = 0;
			try (PreparedStatement s = c.prepareStatement(query)) {
				if (s.execute()) {
					// this is a select, find out how many rows we got
					ResultSet rs = s.getResultSet();
					ResultSetMetaData md = rs.getMetaData();
					while (rs.next()) {
						for (int i = 1; i <= md.getColumnCount(); i++) {
							rows += " ";
							rows += rs.getString(i);
						}
						rows += "\n";
						updatedCount++;
					}
					rs.close();
				} else {
					updatedCount = s.getUpdateCount();
				}
				if (updatedCount == 0) {
					zeroCounts[queryId]++;
				}
				if (updatedCount > 0 && updatedCount % 10 != 0) {
					System.out.println(
							String.format("%s:%s affected %d rows", getTarget().toString(), query, updatedCount));
					if (rows.length() != 0) {
						System.out.println(rows);
					}
				}
			} catch (Throwable t) {
				if (!t.getMessage().contains("deadlock")) {
					t.printStackTrace();
				} else {
					deadlocks++;
				}
			}
		}

	}
}
