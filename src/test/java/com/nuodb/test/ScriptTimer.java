package com.nuodb.test;

import java.sql.Connection;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import com.nuodb.tests.MyTest;
import com.nuodb.tests.workers.SQLScriptExecutor;

public class ScriptTimer extends MyTest {

    private String inputFileName;

    public void setup() {
        readConfiguration();
        Properties properties = getProperties();
        inputFileName = properties.getProperty("nuodb.test.scriptTimerFile", "dummy.test");
    }

    @Test
    public void testScript() {
        setup();
        System.out.println("Using " + inputFileName);
        try (Connection c = connect()) {
            c.setAutoCommit(true);
            SQLScriptExecutor executor = new SQLScriptExecutor(inputFileName, getSchemaName(), null);
            System.out.println("Total duration " + executor.timeScriptExecution(c));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
