package com.nuodb.test;

import static org.junit.Assert.fail;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.nuodb.tests.MyTest;


public class PostgresBatchInsert extends MyTest {

    private static final String DATABASE = "BatchMultiRowTest";
    private static final String SCHEMA = "BatchMultiRowTest";
    private static final int TEST_ITERATIONS = 60;
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    class BatchInsert implements Callable<Exception> {
        private Connection c;
        private long id;
        private PreparedStatement p;

        BatchInsert(Connection c, long id) throws SQLException {
            this.c = c;
            this.id = id;
            this.p = c.prepareStatement("insert into test.a values (?), (?)");
        }

        @Override
        public Exception call() throws Exception {
            System.out.println("starting worker:" + id);
            int[] results = null;
            try {
                for (long i = 0; i < 10; i++) {
                    if (executor.isShutdown()) {
                        System.out.println("early exist from worker " + id);
                        return null;
                    }
                    p.setLong(1, (id << 32) | i);
                    p.setLong(2, i);
                    p.addBatch();
                }
                results = p.executeBatch();
            } catch (BatchUpdateException e) {
                // ignore batch update exceptions due to unique constraint violations
                // e.printStackTrace();
                System.out.println(e.getMessage());
            } catch (SQLException e) {
                executor.shutdown(); // end the test asap
                e.printStackTrace();
                return e;
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Worker " + id + " encountered no error");
            System.out.println(Arrays.toString(results));
            return null;
        }
    }

    @Test
    public void testMultiRowBatchInsert() throws Exception {
        setTarget(TARGET.POSTGRES);
        try (Connection c = openConnection(); Statement stmt = c.createStatement()) {
            stmt.execute("DROP TABLE IF EXISTS TEST.A");
            stmt.execute("CREATE TABLE TEST.A(A BIGINT PRIMARY KEY)");
        } catch (SQLException e) {
            e.printStackTrace();
            fail("Failed to setup test case");
        }

        for (int times = 0; times < 1; times++) {
            final Connection inserter1 = openConnection();
            final Connection inserter2 = openConnection();
            final BatchInsert worker1 = new BatchInsert(inserter1, 1);
            final BatchInsert worker2 = new BatchInsert(inserter2, 2);
            Future<Exception> batchInsertTask1 = executor.submit(worker1);
            Future<Exception> batchInsertTask2 = executor.submit(worker2);

            while (true) {
                if (batchInsertTask1.isDone() && batchInsertTask2.isDone()) {
                    break;
                }
                Thread.sleep(1000);
            }

            final Exception e1 = batchInsertTask1.get(1, TimeUnit.MINUTES);
            final Exception e2 = batchInsertTask2.get(1, TimeUnit.MINUTES);
            Assert.assertNull("Insert task failed with: " + (e1 != null ? e1.getMessage() : ""), e1);
            Assert.assertNull("Insert task failed with: " + (e2 != null ? e2.getMessage() : ""), e2);

            try (Connection c = openConnection(); Statement stmt = c.createStatement()) {
                ResultSet rs = stmt.executeQuery(
                        "select b, count(b) from (select cast(a & 0xFFFFFFFF as int) as b from test.a) group by b having count(b) != 2");
                boolean doFail = false;
                while (rs.next()) {
                    System.out.println("unexpected value in index: " + rs.getString(1) + ", count: " + rs.getString(2));
                    doFail = true;
                }
                if (doFail) {
                    rs = stmt.executeQuery("select a, cast(a & 0xFFFFFFFF as int) from test.a");
                    while (rs.next()) {
                        System.out.println(rs.getString(1) + " : " + rs.getString(2));
                    }

                    fail("Unexpected values in index");
                }

                stmt.execute("truncate table test.a");
            } catch (SQLException e) {
                e.printStackTrace();
                fail("Failed to check test results");
            }
        }
        executor.shutdown();
    }
}
