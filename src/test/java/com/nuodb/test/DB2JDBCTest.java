package com.nuodb.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class DB2JDBCTest extends MyTest {

    @Test
    public void test() {
        setTarget(TARGET.DB2);
        try (Connection c = connect(); Statement s = c.createStatement()) {
            ResultSet rs = s.executeQuery("SELECT 1 + '1' FROM SYSIBM.SYSDUMMY1");
            while (rs.next()) {
                System.out.println(rs.getString(1));
                assertEquals("2", rs.getString(1));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testMultiStatements() {
        setSchemaName("USER");
        setTarget(TARGET.NUODB);
        try (Connection c = connect();
            Statement s1 = c.createStatement();
            Statement s2 = c.createStatement()) {
            ResultSet rs = s1.executeQuery("select * from t");
            s2.execute("insert into t(i) values (1212)");
            int i = 0;
            while(rs.next()) {
                i++;
                if (rs.getString(2) == null) {
                    System.out.println("found a null");
                }
            }
            System.out.println(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
