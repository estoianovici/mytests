package com.nuodb.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class DatabaseCatalogTest extends MyTest {

    protected static void setDebugSync(Connection c, String command) throws SQLException {
        try (Statement stmt = c.createStatement()) {
            stmt.execute("set debug_sync '" + command + "'");
        }
    }

    @Test
    public void testInsertReadCommitted() throws Exception {
        // create test tables
        try (Connection c = openConnection(); Statement s = c.createStatement()) {
            setDebugSync(c, "RESET");
            c.setAutoCommit(true);
            s.execute("drop table if exists t1;");
            s.execute("create table t1(f1 int, f2 int, f3 string)");
            s.execute("insert into t1 values(1, 1, 1), (2, 2, 2), (2, 2, 2), (100000, 100000, 'aaa')");
            c.commit();

            // block gc during the test
            setDebugSync(c, "GarbageCollector::gcCycle WAIT_FOR continue TIMEOUT 0");
        }

        for (int isolationLevel : new int[] { Connection.TRANSACTION_READ_COMMITTED,
                com.nuodb.jdbc.TransactionIsolation.TRANSACTION_CONSISTENT_READ }) {
            try (Connection c = openConnection()) {
                c.setAutoCommit(false);
                c.setTransactionIsolation(isolationLevel);
                try (Statement s = c.createStatement()) {
                    Savepoint savepoint1 = c.setSavepoint();

                    s.execute("alter table t1 add column f4 string");
                    s.execute("insert into t1 values(1, 2, 3, 4)");

                    c.rollback(savepoint1);
                    // test that t1 can be modified again
                    s.execute("alter table t1 add column f4 string");
                    s.execute("insert into t1 values(1, 2, 3, 4)");
                }
                c.rollback();
            }
        }
    }

}
