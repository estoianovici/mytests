package com.nuodb.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class TestPrepareTransaction extends MyTest {

    @Test
    public void testAutocommitSuspendedFlag() throws SQLException {
        this.setConnectionSetup(false);
        try(Connection connection = connect(); Statement stmt = connection.createStatement()) {
            connection.setAutoCommit(false);
            stmt.execute("start transaction isolation level consistent read");
            connection.commit();
            stmt.execute("start transaction isolation level consistent read");
        }
    }

    @Test
    public void test() throws SQLException {
        this.setConnectionSetup(false);
        try(Connection connection = connect()) {
            connection.setAutoCommit(false);
            testIsolationLevel(connection, "SERIALIZABLE");
            commit(connection);
            try (PreparedStatement pstmt = connection.prepareStatement("start transaction isolation level consistent read")) {
                pstmt.execute();
            }
            testIsolationLevel(connection, "CONSISTENT READ");
            
            commit(connection);
            try (PreparedStatement pstmt = connection.prepareStatement("start transaction  isolation level read committed")) {
                pstmt.execute();
            }
            testIsolationLevel(connection, "READ COMMITTED");
        }
    }

    private void commit(Connection connection) throws SQLException {
        try (Statement stmt = connection.createStatement()) {
            stmt.execute("commit");
        }
        
    }

    private void testIsolationLevel(Connection connection, String expected) throws SQLException {
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery("select isolationlevel from system.localtransactions where id = gettransactionid()");
            assertTrue(rs.next());
            assertEquals(expected, rs.getString(1));
            
        }
    }

}
