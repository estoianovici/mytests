package com.nuodb.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;

import com.nuodb.tests.DDLScriptTest;

public class DDLScriptTestTest {

	@Test
	public void testLoadProperties() {
		URL url = getClass().getResource("/MyTest.properties");
		String propertiesFile = url.getFile();
		assertTrue(propertiesFile != null);

		System.setProperty("nuodb.test.propertiesFile", propertiesFile);
		DDLScriptTest test = new DDLScriptTest();
		assertEquals("test_host", test.getHostName());
		assertEquals("test_database", test.getDatabaseName());
		assertEquals("test_schema", test.getSchemaName());
		assertEquals("test_user", test.getUserName());
		assertEquals("test_password", test.getPassword());
		assertEquals("11111", test.getPort());
		assertEquals("test_input_file", test.getInputFileName());
		assertEquals("test_output_file", test.getOutputFileName());
		assertEquals("1001", test.getLoggerInterval() + "");
	}

	@Test
	public void testDefaultProperties() {
		System.clearProperty("nuodb.test.propertiesFile");
		DDLScriptTest test = new DDLScriptTest();
		assertEquals("localhost", test.getHostName());
		assertEquals("test", test.getDatabaseName());
		assertEquals("slowddltest", test.getSchemaName());
		assertEquals("dba", test.getUserName());
		assertEquals("dba", test.getPassword());
		assertEquals("48004", test.getPort());
		assertEquals("input.sql", test.getInputFileName());
		assertEquals("duration.log", test.getOutputFileName());
		assertEquals("1000", test.getLoggerInterval() + "");
	}


}
