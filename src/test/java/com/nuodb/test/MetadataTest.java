package com.nuodb.test;

import com.nuodb.tests.MyTest;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class MetadataTest extends MyTest {

    @Test
    public void testMetadata() {
        String query = "SELECT 'abc' AS XYZ, 123 AS `123` FROM DUAL;";
        try (Connection c = connect();
             PreparedStatement pstmt = c.prepareStatement(query)) {

            ResultSetMetaData metaData = pstmt.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                System.out.println(metaData.getColumnName(i));
                assertFalse(metaData.getColumnName(i) == "");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    @Test
    public void testSQLServerMetadata() {
        String query = "SELECT 'abc' AS XYZ, 123";
        this.setTarget(TARGET.SQLSERVER);
        try (Connection c = connect();
             PreparedStatement pstmt = c.prepareStatement(query)) {

            ResultSetMetaData metaData = pstmt.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                System.out.println(metaData.getColumnName(i));
                assertFalse(metaData.getColumnName(i) == "");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
