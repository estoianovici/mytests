package com.nuodb.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class TriggerDeadlockTest extends MyTest {
    static final String[] schema = {
            "create table t1(i int, j int)",
            "create table t2(i int, j int)",
            "create table ta(i int, j int)",
            "create table tb(i int, j int)",
            "insert into ta values(1, 1)",
            "insert into tb values(1, 1)",
            "create trigger trg1 for t1 before insert as "
                    + "update ta set i = j; "
                    + "update tb set i = j; "
                    + " end_trigger",
            "create trigger trg2 for t2 before insert as "
                    + "update tb set i = j; "
                    + "update ta set i = j; "
                    + " end_trigger",
            "commit;"
    };

    private volatile static boolean keepRunning = true;

    @Test
    public void testDeadlock() throws Exception {
        setup();
        keepRunning = true;
        Thread threads[] = new Thread[2];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(i % 2 == 0 ? new Inserter("t1") : new Inserter("t2"));
            threads[i].start();
        }
        Thread.sleep(120 * 1000);
        keepRunning = false;
        for (int i = 0; i < threads.length; i++) {
            threads[i].join();
        }
    }

    class Inserter implements Runnable {
        private String tableName;

        public Inserter(String tableName) {
            this.tableName = tableName;
        }

        @Override
        public void run() {
            try (Connection c = connect()) {
                c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                c.setAutoCommit(false);
                while (keepRunning) {
                    try (PreparedStatement stmt = c.prepareStatement("INSERT INTO " + tableName + " VALUES(?, ?)")) {
                        stmt.setInt(1, (int) Math.random());
                        stmt.setInt(2, (int) Math.random());
                        stmt.execute();
                        if (stmt.getUpdateCount() > 0) {
                            System.out.println("inserted " + tableName);
                        }
                        c.commit();
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

    private void setup() throws Exception {
        try (Connection c = connect()) {
            runQueries(c, new String[] { "drop schema " + getSchemaName() + " cascade" });
            runQueries(c, schema);
            c.commit();
        }
    }
}
