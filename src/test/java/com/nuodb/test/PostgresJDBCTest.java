package com.nuodb.test;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.junit.Test;

public class PostgresJDBCTest {

	@Test
	public void testJdbcMetadata() {
		final String tableDDL = "CREATE TABLE TEST_TYPES (ID SERIAL, " + "STR1 varchar(255) DEFAULT NULL, "
				+ "STR2 varchar(32), " + "CHAR1 CHAR, " + "CLOB1 VARCHAR(40), " + "SMALLINT1 SMALLINT, "
				+ "BIGINT1 BIGINT, " + "DOUBLE1 DOUBLE PRECISION, " + "DATE1 DATE, " + "TIME1 TIME, "
				+ "TIMESTAMP1 TIMESTAMP, " + "BOOLEAN1 BOOLEAN, " + "PRIMARY KEY (ID))";

		try (Connection c = connect(); Statement s = c.createStatement()) {
			s.execute("drop table if exists test_types");
			s.execute(tableDDL);
			/*
			 * PreparedStatement pstmt = c.prepareStatement(insertStmt);
			 * pstmt.setString(2, "FOO"); pstmt.setString(3, "BAR");
			 * pstmt.setString(4, "chr"); pstmt.setString(5, "clb");
			 * pstmt.setInt(6, 2); // smallint pstmt.setInt(7, 3); // bigint
			 * pstmt.setInt(8, 4); // double pstmt.setBoolean(12, true);
			 * pstmt.execute(); pstmt.close();
			 */

			DatabaseMetaData md = c.getMetaData();
			printResultSet(md.getTables(null, null, "test_types", null));
			printResultSet(md.getColumns(null, null, "test_types", null));
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	@Test
	public void testScripts() {
		final String tableDDL = "";

		try (Connection c = connect(); Statement s = c.createStatement()) {
			s.execute("drop table if exists public.t");
			s.execute("create table public.t(i int, j int)");
			s.execute(tableDDL);
			s.close();
            try (PreparedStatement pstmt = c.prepareStatement("insert into t values(?, ?)")) {
                for (int i = 1; i < 101; i++) {
                    pstmt.setInt(1, i);
                    pstmt.setInt(2, i);
                    pstmt.executeUpdate();
                }
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	private Connection connect() throws SQLException {
		String url = "jdbc:postgresql://localhost:5432/postgres";
		Properties props = new Properties();
		props.setProperty("user", "postgres");
		props.setProperty("password", "admin");
		return DriverManager.getConnection(url, props);
	}

	private void printResultSet(ResultSet rs) throws SQLException {
		if (!rs.next()) {
			System.out.println("failed");
			rs.close();
			return;
		}
		ResultSetMetaData rsMetaData = rs.getMetaData();
		do {
			for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
				System.out.print(rsMetaData.getColumnName(i));
				System.out.println(": " + rs.getString(i));
			}
		} while (rs.next());
		rs.close();
	}
}
