package com.nuodb.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class DeadlockStormTest extends MyTest {
	private final static int NUM_TABLES = 15;
	private final static int NUM_THREADS = 100;
	Random r = new Random(System.currentTimeMillis());

	@Test
	public void test() throws Exception {
		// setup();
		System.out.println("DONE SETUP");
		Thread workers[] = new Thread[NUM_THREADS];
		for (int i = 0; i < workers.length; i++) {
			workers[i] = new Thread(new Worker(i));
			workers[i].start();
		}

		for (int i = 0; i < workers.length; i++) {
			workers[i].join();
		}
	}

	private void setup() throws SQLException {
		try (Connection c = connect(); Statement s = c.createStatement()) {
			for (int i = 0; i < NUM_TABLES; i++) {
				s.execute(String.format("CREATE TABLE t%d(i int, j int)", i));
				s.execute(String
						.format("INSERT INTO t%d SELECT rownum(), rownum() from system.fields a, system.fields b", i));
				s.execute(String.format("create index i_t%d_i on t%d(i)", i, i));
			}
			c.commit();
		}
	}

	class Worker implements Runnable {
		public Worker(int id) {
			this.id = id;
		}

		@Override
		public void run() {
			int runs = 0;
			while (!Thread.interrupted()) {
				runTest();
				runs++;
				if (runs % 10 == 0) {
					log("" + runs);
				}
			}
		}

		private void runTest() {
			int tables = r.nextInt(NUM_TABLES);
			Map<Integer, Integer> tablesMap = new HashMap<>();
			try (Connection c = connect()) {
				for (int i = 0; i < tables; i++) {
					int table = r.nextInt(NUM_TABLES + 1);
					if (r.nextInt() % 4 == 0) {
						if (tablesMap.containsKey(table) && tablesMap.get(table) == 0) {
							log("X_LOCKING S_LOCKED TABLE");
						}
						tablesMap.put(table, 1);
						lockTable(c, table);
					} else {
						if (tablesMap.containsKey(table) && tablesMap.get(table) == 1) {
							log("S_LOCKING X_LOCKED TABLE");
						} else {
							tablesMap.put(table, 0);
						}
						lockRows(c, table);
					}
				}
				c.commit();
			} catch (Throwable t) {
				log(t.getMessage());
			}
		}

		private void lockRows(Connection c, int table) throws SQLException {
			int factor = r.nextInt(NUM_THREADS + 1);
			try (Statement s = c.createStatement()) {
				ResultSet rs = s
						.executeQuery(String.format("SELECT * FROM t%d WHERE i %% %d = 0 FOR UPDATE", table, factor));
				while (rs.next())
					;
			} catch (SQLException t) {
				c.rollback();
				throw t;
			}
		}

		private void lockTable(Connection c, int table) throws SQLException {
			try (Statement s = c.createStatement()) {
				s.execute(String.format("LOCK TABLE t%d", table));
			} catch (SQLException t) {
				c.rollback();
				throw t;
			}
		}

		private void log(String msg) {
			System.out.println(id + ": " + msg);
		}

		private int id;
	}
}
