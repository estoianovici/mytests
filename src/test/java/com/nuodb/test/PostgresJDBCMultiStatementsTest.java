package com.nuodb.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class PostgresJDBCMultiStatementsTest extends MyTest {

    @Test
    public void testMultiStatements() {
        setTarget(TARGET.POSTGRES);
        setTarget(TARGET.DB2);
        try (Connection c = connect(); Statement s = c.createStatement()) {
            c.setAutoCommit(true);
            s.execute("drop table if exists t");
            s.execute("create table t(i int, j int)");
            s.close();
            try (PreparedStatement pstmt = c.prepareStatement("insert into t values(?, ?)")) {
                for (int i = 1; i < 5; i++) {
                    pstmt.setInt(1, i);
                    pstmt.setInt(2, i);
                    pstmt.executeUpdate();
                }
            }
            try {
                s.execute("select * from t");
                assertNotEquals(null, s.getResultSet());
                s.getResultSet().close();

                s.execute("select * from t; commit");
                assertEquals(null, s.getResultSet());
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                assertTrue(e.getMessage(), e.getMessage().contains("has been closed") || e.getMessage().contains("statement is closed"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
