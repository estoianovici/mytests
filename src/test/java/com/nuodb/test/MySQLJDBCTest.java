package com.nuodb.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class MySQLJDBCTest extends MyTest {

    @Test
    public void test() {
        setTarget(TARGET.MySQL);
        try (Connection c = connect(); Statement s = c.createStatement()) {
            ResultSet rs = s.executeQuery("SELECT 1 + '1'");
            while (rs.next()) {
                System.out.println(rs.getString(1));
                assertEquals("2.0", rs.getString(1));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            assertTrue(e.getMessage(), false);
        }
    }

}
