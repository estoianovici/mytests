package com.nuodb.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class OracleJDBCTest extends MyTest {

    @Test
    public void test() {
        setTarget(TARGET.ORACLE);
        try (Connection c = connect(); Statement s = c.createStatement()) {
            s.execute("DROP USER MigratorTest");
            s.execute("CREATE USER MigratorTest IDENTIFIED BY root quota 20m on system");
            s.execute("GRANT create session TO migratortest");
            s.execute("GRANT create table TO migratortest");
            s.execute("GRANT create view TO migratortest");
            s.execute("GRANT create any trigger TO migratortest");
            s.execute("GRANT create any procedure TO migratortest");
            s.execute("GRANT create sequence TO migratortest");
            s.execute("GRANT create synonym TO migratortest");
            ResultSet rs = s.executeQuery("select sys_context('USERENV', 'CURRENT_SCHEMA') from dual");
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
            rs.close();

        } catch (SQLException e) {
            e.printStackTrace();
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void manualTest() {
        String url = "jdbc:oracle:thin:@localhost:1521:xe";
        Properties props = new Properties();
        props.setProperty("user", "MigratorTest");
        props.setProperty("password", "root");
        try (Connection c = DriverManager.getConnection(url, props); Statement s = c.createStatement()) {
            ResultSet rs = s.executeQuery("select sys_context('USERENV', 'CURRENT_SCHEMA') from dual");
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
            rs.close();

            //            s.execute("CREATE TABLE t1(i INT, n NUMBER, n1 NUMBER(10, 2))");
            //            s.execute("INSERT INTO t1 VALUES(1, 1.1, 2.01)");
            rs = s.executeQuery("select * from t1");
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testMigratorQuery() {
        final String query = "SELECT NULL AS TABLE_CAT, O.OWNER AS TABLE_SCHEM, O.OBJECT_NAME AS TABLE_NAME, O.OBJECT_TYPE AS TABLE_TYPE, C.COMMENTS AS REMARKS FROM ALL_OBJECTS O, ALL_TAB_COMMENTS C WHERE O.OWNER = C.OWNER AND O.OBJECT_NAME = C.TABLE_NAME AND O.OWNER='MIGRATORTEST' AND O.OBJECT_TYPE='TABLE'";
        String url = "jdbc:oracle:thin:@localhost:1521:xe";
        Properties props = new Properties();
        props.setProperty("user", "MigratorTest");
        props.setProperty("password", "root");
        try (Connection c = DriverManager.getConnection(url, props); Statement s = c.createStatement()) {
            ResultSet rs = s.executeQuery(query);
            ResultSetMetaData metadata = rs.getMetaData();
            while (rs.next()) {
                for (int i = 0; i < metadata.getColumnCount(); i++) {
                    System.out.println(metadata.getColumnName(i + 1) + ":" + rs.getString(i + 1));
                }
            }
            rs.close();

            rs = s.executeQuery("select * from t1");
            while (rs.next()) {
                System.out.println(rs.getString(1));
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
            assertTrue(e.getMessage(), false);
        }
    }
}
