package com.nuodb.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Assert;
import org.junit.Test;

import com.nuodb.tests.MyTest;

public class NuoDBBatchInsertDebug extends MyTest {
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    private final static int BATCH_SIZE = 10000;
    private final static int WORKERS = 40;

    class BatchResult {
        public long workerId;
        public int failed;
    }

    class BatchInsert implements Callable<BatchResult> {
        private static final String insertQuery = "insert into lake.rel_tip_dom_his values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        private BatchResult result;
        private String filePath;

        BatchInsert(String filePath, long id) {
            this.result = new BatchResult();
            this.result.workerId = id;
            this.result.failed = 0;
            this.filePath = filePath;
        }

        void executeBatch(PreparedStatement stmt) throws SQLException {
            try {
                int[] counts = stmt.executeBatch();
                for (int cnt : counts) {
                    if (cnt == PreparedStatement.EXECUTE_FAILED) {
                        result.failed++;
                    }
                }
            } catch (SQLException e) {
                System.out.println(result.workerId + ": execute failed with " + e.getMessage());
                stmt.getConnection().commit();
            }
        }

        @Override
        public BatchResult call() throws Exception {
            try (Connection c = connect(); PreparedStatement stmt = c.prepareStatement(insertQuery)) {
                c.setAutoCommit(false);
                c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                int cnt = 0;
                String row;
                BufferedReader csvReader = new BufferedReader(new FileReader(filePath));
                while ((row = csvReader.readLine()) != null) {
                    String[] data = row.split(",");
                    for (int i = 0; i < data.length; i++) {
                        if (data[i].length() == 0) {
                            stmt.setNull(i + 1, java.sql.Types.VARCHAR);
                        } else {
                            stmt.setString(i + 1, data[i]);
                        }
                    }
                    stmt.addBatch();
                    cnt++;
                    if (cnt % BATCH_SIZE == 0) {
                        executeBatch(stmt);
                        c.commit();
                        System.out.println(result.workerId + ": inserted " + cnt);
                    }
                }
                executeBatch(stmt);
                csvReader.close();
                c.commit();
                return result;
            }
        }
    }

    @Test
    public void testDuplicates() throws Exception {
        // setup();

        List<Future<BatchResult>> futures = new ArrayList<>();
        for (int i = 1; i < WORKERS; i++) {
            // final BatchInsert worker1 = new BatchInsert("/home/estoianovici/work/bugdata/db29300/out" + i + ".csv", i);
            final BatchInsert worker1 = new BatchInsert("/home/estoianovici/work/bugdata/db29300/suffix_bis.csv", i);
            futures.add(executor.submit(worker1));
        }

        List<BatchResult> results = new ArrayList<>();
        for (Future<BatchResult> fut : futures) {
            results.add(fut.get());
        }
        for (BatchResult result : results) {
            System.out.println(result.workerId + ": failed " + result.failed);
        }
        executor.shutdown();

        try (Connection c = openConnection(); Statement s1 = c.createStatement()) {
            ResultSet rs = s1.executeQuery(
                    "select /*+ skip_index(REL_TIP_DOM_HIS) */ E0712_TIPO_PER, E0712_COD_PERS, E0712_TIPODOM, E0712_FECH_INI, E0712_TIMALTA, count(1) from EXA01.REL_TIP_DOM_HIS GROUP BY E0712_TIPO_PER, E0712_COD_PERS, E0712_TIPODOM, E0712_FECH_INI, E0712_TIMALTA HAVING count(1) > 1");
            Assert.assertFalse(rs.next());
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setup() throws Exception {
        String[] queries = { "drop schema LAKE cascade",
                "drop schema EXA01 cascade",
                "CREATE SCHEMA EXA01",

                "CREATE TABLE EXA01.REL_TIP_DOM_HIS (\n" +
                        "            E0712_TIPO_PER char(1) NOT NULL,\n" +
                        "            E0712_COD_PERS integer NOT NULL,\n" +
                        "            E0712_TIPODOM char(2) NOT NULL,\n" +
                        "            E0712_FECH_INI date NOT NULL,\n" +
                        "            E0712_TIMALTA timestamp NOT NULL,\n" +
                        "            E0712_FECH_FIN date,\n" +
                        "            E0712_NRODOM integer,\n" +
                        "            E0712_USULTACT char(8),\n" +
                        "            E0712_EMULTACT char(4),\n" +
                        "            E0712_CEULTACT char(4),\n" +
                        "            E0712_FEULTACT date,\n" +
                        "            E0712_HOR_ULTA time,\n" +
                        "            CH_TIMESTAMP timestamp,\n" +
                        "            CH_DELETE_FLAG boolean,\n" +
                        "            STREAM_TIMESTAMP timestamp,\n" +
                        "      CONSTRAINT \"REL_TIP_DOM_HIS..PRIMARY_KEY\" PRIMARY KEY (E0712_TIPO_PER, E0712_COD_PERS, E0712_TIPODOM, E0712_FECH_INI, E0712_TIMALTA)\n"
                        +
                        ")",

                "CREATE INDEX IDX_REL_TIP_DOM_HIS_X0130748 ON EXA01.REL_TIP_DOM_HIS (E0712_TIPO_PER, E0712_COD_PERS, E0712_NRODOM)",
                "CREATE INDEX IDX_REL_TIP_DOM_HIS_X9030748 ON EXA01.REL_TIP_DOM_HIS (E0712_TIPO_PER, E0712_COD_PERS, E0712_FECH_FIN, E0712_NRODOM, E0712_TIPODOM)",

                "CREATE SCHEMA LAKE",

                "CREATE TABLE LAKE.REL_TIP_DOM_HIS (\n" +
                        "E0712_TIPO_PER char(1) NOT NULL,\n" +
                        "            E0712_COD_PERS integer NOT NULL,\n" +
                        "            E0712_TIPODOM char(2) NOT NULL,\n" +
                        "            E0712_FECH_INI date NOT NULL,\n" +
                        "            E0712_TIMALTA timestamp NOT NULL,\n" +
                        "            E0712_FECH_FIN date,\n" +
                        "            E0712_NRODOM integer,\n" +
                        "            E0712_USULTACT char(8),\n" +
                        "            E0712_EMULTACT char(4),\n" +
                        "            E0712_CEULTACT char(4),\n" +
                        "            E0712_FEULTACT date,\n" +
                        "            E0712_HOR_ULTA time,\n" +
                        "            CH_TIMESTAMP timestamp,\n" +
                        "            CH_DELETE_FLAG boolean,\n" +
                        "            STREAM_TIMESTAMP timestamp\n" +
                        "    )",

                "CREATE TRIGGER TRG_REL_TIP_DOM_HIS FOR LAKE.REL_TIP_DOM_HIS  BEFORE INSERT\n" +
                        "AS \n" +
                        "VAR old_timestamp timestamp = null;\n" +
                        "    NEW.STREAM_TIMESTAMP = CURRENT_TIMESTAMP;\n" +
                        "    NEW.CH_TIMESTAMP = COALESCE(NEW.CH_TIMESTAMP,'1999-01-01 09:00:00');\n" +
                        "    old_timestamp = (select CH_TIMESTAMP from EXA01.REL_TIP_DOM_HIS WHERE E0712_TIPO_PER=NEW.E0712_TIPO_PER AND E0712_COD_PERS=NEW.E0712_COD_PERS AND E0712_TIPODOM=NEW.E0712_TIPODOM AND E0712_FECH_INI=NEW.E0712_FECH_INI AND E0712_TIMALTA=NEW.E0712_TIMALTA);\n"
                        +
                        "    IF (old_timestamp IS NULL)\n" +
                        "        TRY\n" +
                        "            INSERT INTO EXA01.REL_TIP_DOM_HIS (E0712_TIPO_PER,E0712_COD_PERS,E0712_TIPODOM,E0712_FECH_INI,E0712_TIMALTA,E0712_FECH_FIN,E0712_NRODOM,E0712_USULTACT,E0712_EMULTACT,E0712_CEULTACT,E0712_FEULTACT,E0712_HOR_ULTA,CH_TIMESTAMP,CH_DELETE_FLAG,STREAM_TIMESTAMP)\n"
                        +
                        "            VALUES (NEW.E0712_TIPO_PER,NEW.E0712_COD_PERS,NEW.E0712_TIPODOM,NEW.E0712_FECH_INI,NEW.E0712_TIMALTA,NEW.E0712_FECH_FIN,NEW.E0712_NRODOM,NEW.E0712_USULTACT,NEW.E0712_EMULTACT,NEW.E0712_CEULTACT,NEW.E0712_FEULTACT,NEW.E0712_HOR_ULTA,NEW.CH_TIMESTAMP,NEW.CH_DELETE_FLAG,NEW.STREAM_TIMESTAMP);\n"
                        +
                        "        CATCH(error)\n" +
                        "            IF (UPPER(error) not like UPPER('Duplicate%'))\n" +
                        "                THROW error;\n" +
                        "            END_IF;\n" +
                        "        END_TRY;\n" +
                        "    ELSE\n" +
                        "        IF (NEW.CH_TIMESTAMP > old_timestamp)\n" +
                        "            UPDATE EXA01.REL_TIP_DOM_HIS SET E0712_FECH_FIN=NEW.E0712_FECH_FIN,\n" +
                        "                E0712_NRODOM=NEW.E0712_NRODOM,\n" +
                        "                E0712_USULTACT=NEW.E0712_USULTACT,\n" +
                        "                E0712_EMULTACT=NEW.E0712_EMULTACT,\n" +
                        "                E0712_CEULTACT=NEW.E0712_CEULTACT,\n" +
                        "                E0712_FEULTACT=NEW.E0712_FEULTACT,\n" +
                        "                E0712_HOR_ULTA=NEW.E0712_HOR_ULTA,\n" +
                        "                CH_TIMESTAMP=NEW.CH_TIMESTAMP,\n" +
                        "                CH_DELETE_FLAG=NEW.CH_DELETE_FLAG,\n" +
                        "                STREAM_TIMESTAMP=NEW.STREAM_TIMESTAMP\n" +
                        "            WHERE E0712_TIPO_PER=NEW.E0712_TIPO_PER AND E0712_COD_PERS=NEW.E0712_COD_PERS AND E0712_TIPODOM=NEW.E0712_TIPODOM AND E0712_FECH_INI=NEW.E0712_FECH_INI AND E0712_TIMALTA=NEW.E0712_TIMALTA AND CH_TIMESTAMP < NEW.CH_TIMESTAMP;\n"
                        +
                        "        END_IF;\n" +
                        "    END_IF;\n" +
                        "END_TRIGGER" };
        try (Connection c = openConnection(); Statement s1 = c.createStatement()) {
            runQueries(c, queries);
            c.commit();
        }
    }
}
