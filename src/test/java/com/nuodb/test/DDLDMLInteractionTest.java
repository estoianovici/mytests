package com.nuodb.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class DDLDMLInteractionTest extends MyTest {
    private static final String SCHEMA = "DDLDMLSCHEMA";
    private static final String TABLE = "DDLDMLTABLE";
    private static final TARGET targets[] = { TARGET.SQLSERVER, TARGET.POSTGRES, TARGET.NUODB };

    private String[] testSetupQueries(TARGET target) {
        switch (getTarget()) {
        case NUODB: {
            String queries[] = {
                    "set system property transactional_locks = true",
                    "set system property transactional_ddl = true",
                    String.format("drop table if exists %s.%s", SCHEMA, TABLE),
                    String.format("create table %s.%s(i int, j int)", SCHEMA, TABLE)
            };
            return queries;
        }
        case DB2: {
            String queries[] = {
                    String.format("drop table %s.%s", SCHEMA, TABLE),
                    String.format("create table %s.%s(i int, j int)", SCHEMA, TABLE)
            };
            return queries;
        }
        default: {
            String queries[] = {
                    String.format("drop table if exists %s.%s", SCHEMA, TABLE),
                    String.format("create table %s.%s(i int, j int)", SCHEMA, TABLE)
            };
            return queries;
        }
        }
    };

    @Test
    public void testInsertReadCommitted() throws Exception {
        System.out.println("========== TESTING READ COMMITTED ==========");
        for (TARGET target : targets) {
            String ddlQuery = String.format("alter table %s.%s add new_col int", SCHEMA, TABLE);
            System.out.println("Testing " + target.toString());
            int isolationLevel = getTargetIsolationLevel(target, Connection.TRANSACTION_READ_COMMITTED);
            setTarget(target);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(i) values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(i, new_col) values(1, 1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set new_col = 1 where i > 0", SCHEMA, TABLE), ddlQuery);

            ddlQuery = String.format("alter table %s.%s drop column j", SCHEMA, TABLE);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(j) values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(i) values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set j = 1 where i > 0", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set i = 1 where i > 0", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set i = 1 where j > 0", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("select j from %s.%s", SCHEMA, TABLE), ddlQuery);
        }
    }

    @Test
    public void testInsertRepeatableRead() throws Exception {
        System.out.println("========== TESTING REPEATABLE READ ==========");
        for (TARGET target : targets) {
            System.out.println("Testing " + target.toString());
            String ddlQuery = String.format("alter table %s.%s add new_col int", SCHEMA, TABLE);
            int isolationLevel = getTargetIsolationLevel(target, Connection.TRANSACTION_REPEATABLE_READ);
            setTarget(target);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(i) values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(i, new_col) values(1, 1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set new_col = 1 where i > 0", SCHEMA, TABLE), ddlQuery);

            ddlQuery = String.format("alter table %s.%s drop column j", SCHEMA, TABLE);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(j) values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("insert into %s.%s(i) values(1)", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set j = 1 where i > 0", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set i = 1 where i > 0", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("update %s.%s set i = 1 where j > 0", SCHEMA, TABLE), ddlQuery);
            testDMLDDLConflict(isolationLevel, String.format("select j from %s.%s", SCHEMA, TABLE), ddlQuery);
        }
    }

    private void testDMLDDLConflict(int isolationLevel, String dmlQuery, String ddlQuery) throws Exception {
        setupTest();
        System.out.println(String.format("%s: Testing {%s} vs {%s}",getTarget().toString(), dmlQuery, ddlQuery));
        ExecutorService executor = Executors.newCachedThreadPool();
        try (final Connection ddlConnection = connect(isolationLevel);
                final Statement ddlStatement = ddlConnection.createStatement()) {
            ddlStatement.execute(ddlQuery);
            Future<QueryResult> futureResult = executor.submit(new BlockingQuery(isolationLevel, dmlQuery));
            ddlConnection.commit();
            QueryResult result = futureResult.get();
            System.out.println(result.toString());
        } catch (Exception e) {
            System.out.println(getTarget().toString() + ": " + e.getMessage());
        } finally {
            executor.shutdownNow();
        }
    }

    private Connection connect(int isolationLevel) throws SQLException {
        Connection c = super.connect();
        c.setAutoCommit(false);
        c.setTransactionIsolation(isolationLevel);
        return c;
    }

    private void setupTest() throws SQLException {
        try (final Connection c = connect(Connection.TRANSACTION_READ_COMMITTED)) {
            for (String query : testSetupQueries(getTarget())) {
                try (final Statement stmt = c.createStatement()) {
                    stmt.execute(query);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    c.rollback();
                }
            }
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }

    private int getTargetIsolationLevel(TARGET target, int level) {
        if (target == TARGET.NUODB && level == Connection.TRANSACTION_REPEATABLE_READ) {
        }
        return level;
    }

    /**
     * Small class describing the result of a query;
     */
    class QueryResult {
        public QueryResult(PreparedStatement s) throws Exception {
            try {
                hasResultSet = s.execute();
                if (hasResultSet) {
                    ResultSet rs = s.getResultSet();
                    ResultSetMetaData metadata = rs.getMetaData();
                    rows = new ArrayList<ArrayList<String>>();
                    while (rs.next()) {
                        rows.add(new ArrayList<String>());
                        ArrayList<String> row = rows.get(rows.size() - 1);
                        for (int i = 0; i < metadata.getColumnCount(); i++) {
                            row.add(rs.getString(i + 1));
                        }
                    }
                    rs.close();
                } else {
                    rowsAffected = s.getUpdateCount();
                }
            } catch (Exception e) {
                thrown = e;
                s.getConnection().rollback();
            }
        }

        public QueryResult(Exception e) {
            thrown = e;
        }

        public boolean isHasResultSet() {
            return hasResultSet;
        }

        public int getRowsAffected() {
            return rowsAffected;
        }

        public ArrayList<ArrayList<String>> getRows() {
            return rows;
        }

        public Exception getThrown() {
            return thrown;
        }

        public boolean threwException() {
            return thrown != null;
        }

        private boolean hasResultSet;
        private int rowsAffected;
        private ArrayList<ArrayList<String>> rows;
        private Exception thrown;

        @Override
        public String toString() {
            if (threwException()) {
                return getTarget().toString() + ": " + thrown.getMessage();
            } else if (isHasResultSet()) {
                StringBuffer sb = new StringBuffer("");
                for (ArrayList<String> row : rows) {
                    if (row.size() == 0) {
                        continue;
                    }
                    sb.append(row.get(0));
                    for (int i = 1; i < row.size(); i++) {
                        sb.append("\t, ").append(row.get(i));
                    }
                    sb.append('\n');
                }
                return sb.toString();
            } else {
                return "affected " + rowsAffected;
            }
        }
    }

    class BlockingQuery implements Callable<QueryResult> {

        private String query;
        private int isolationLevel;

        public BlockingQuery(int isolationLevel, String query) {
            this.isolationLevel = isolationLevel;
            this.query = query;
        }

        @Override
        public QueryResult call() throws Exception {
            System.out.println("Executing: " + query);
            try (Connection c = connect(isolationLevel)) {
                try (PreparedStatement s = c.prepareStatement(query)) {
                    return new QueryResult(s);
                } catch (Exception e) {
                    c.rollback();
                    throw e;
                }
            } catch (Exception e) {
                return new QueryResult(e);
            }
        }

    }

}
