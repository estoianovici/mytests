package com.nuodb.test;

import java.sql.Connection;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class CreateOrReplaceTests extends MyTest {
    volatile static boolean keepRunning;
    int duration = 5;
    int threadCount = 5;
    int counts[] = new int[threadCount];

    @Test
    public void testSQLServerCreateOrReplace() {
        System.out.println(TARGET.SQLSERVER);
        setTarget(TARGET.SQLSERVER);
        runTest();
    }

    @Test
    public void testNuoDBCreateOrReplace() {
        System.out.println(TARGET.NUODB);
        setTarget(TARGET.NUODB);
        runTest();
    }

    private void runTest() {
        try {
            Thread threads[] = new Thread[threadCount];
            keepRunning = true;
            for (int i = 0; i < threads.length; i++) {
                threads[i] = new Thread(new Worker(i));
                threads[i].start();
            }

            waitAndDrop(duration);
            keepRunning = false;

            for (int i = 0; i < threads.length; i++) {
                threads[i].join();
                System.out.println(i + ": " + counts[i]);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class Worker implements Runnable {
        private int id;

        public Worker(int id) {
            counts[id] = 0;
            this.id = id;
        }

        @Override
        public void run() {
            try (Connection c = connect()) {
                c.setAutoCommit(false);
                c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                while (keepRunning) {
                    try (Statement stmt = c.createStatement()) {
                        stmt.execute(getQuery());
                        c.commit();
                        counts[id]++;
                    } catch (Throwable t) {
                        System.out.println(id + ": " + t.getMessage());
                        c.rollback();
                    }
                }
            } catch (Throwable t) {
                System.out.println(id + " exiting: " + t.getMessage());
            }
        }

    }

    public void waitAndDrop(int timeout) {
        while (timeout >= 0) {
            try (Connection c = connect(); Statement stmt = c.createStatement()) {
                c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                System.out.println("main: tick(" + timeout + ")" + timeQuery(c, "drop function fntest"));
                c.commit();
                Thread.sleep(1000);
            } catch (Throwable t) {
                System.out.println("main: " + t.getMessage());
            }
            timeout--;
        }
    }

    public String getQuery() {
        switch (getTarget()) {
        case SQLSERVER:
            return "create or alter function fntest() returns int as begin return (1) end";

        case NUODB:
            return "create or replace function fntest() returns int as return 1; end_function";

        default:
            return "";
        }
    }
}
