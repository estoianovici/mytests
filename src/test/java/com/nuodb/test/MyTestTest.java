package com.nuodb.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class MyTestTest {

	@Test
	public void testLoadProperties() {
		URL url = getClass().getResource("/MyTest.properties");
		String propertiesFile = url.getFile();
		assertTrue(propertiesFile != null);

		System.setProperty("nuodb.test.propertiesFile", propertiesFile);
		MyTest test = new MyTest();
		test.readConfiguration();
		assertEquals("test_host", test.getHostName());
		assertEquals("test_database", test.getDatabaseName());
		assertEquals("test_schema", test.getSchemaName());
		assertEquals("test_user", test.getUserName());
		assertEquals("test_password", test.getPassword());
		assertEquals("11111", test.getPort());
	}

	@Test
	public void testDefaultProperties() {
		System.clearProperty("nuodb.test.propertiesFile");
		MyTest test = new MyTest();
		test.readConfiguration();
		assertEquals("localhost", test.getHostName());
		assertEquals("test", test.getDatabaseName());
		assertEquals("slowddltest", test.getSchemaName());
		assertEquals("dba", test.getUserName());
		assertEquals("dba", test.getPassword());
		assertEquals("48004", test.getPort());

	}

}
