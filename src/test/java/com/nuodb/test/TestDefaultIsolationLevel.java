package com.nuodb.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import org.junit.Test;

public class TestDefaultIsolationLevel {

    @Test
    public void test() throws Exception {
        String url = String.format("jdbc:com.nuodb://localhost/test");
        Properties properties = new Properties();
        properties.setProperty("user", "dba");
        properties.setProperty("password", "dba");
        Connection c = DriverManager.getConnection(url, properties);
        c.setAutoCommit(false);
        Statement s = c.createStatement();
        ResultSet rs = s.executeQuery("select * from system.localtransactions");
        while(rs.next()) {
            for (int i = 1; i<= 15; i++) {
                System.out.print(rs.getString(i) + "\t");
            }
            System.out.println();
        }
    }

}
