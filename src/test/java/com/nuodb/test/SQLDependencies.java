package com.nuodb.test;

import java.sql.Connection;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class SQLDependencies extends MyTest {

    @Test
    public void testSQLServerDependencies() {
        setTarget(TARGET.SQLSERVER);
        try (Connection c = connect()) {
            runQueries(c, "drop table t", "create table t(i int)", "create view v as select * from t", "drop table t",
                    "select * from v");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPostgresqlDependencies() {
        setTarget(TARGET.POSTGRES);
        try (Connection c = connect()) {
            runQueries(c, "drop table if exists t", "create table t(i int)", "create view v as select * from t", "drop table t",
                    "select * from v");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testOracleDependencies() {
        setTarget(TARGET.ORACLE);
        try (Connection c = connect()) {
            runQueries(c, "create table t(i int)", "create view v as select * from t", "drop table t",
                    "select * from v");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
