package com.nuodb.test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.BeforeClass;
import org.junit.Test;

import com.nuodb.tests.MyTest;

public class SQLServerJDBCTest extends MyTest {

    public static String tableDDL = "create table t(i1 integer, b1 bigint, s1 varchar(32), "
            + "n1 numeric(5, 2), n2 numeric(10, 2), n3 numeric(20, 3))";

    public static String insertStmt = "insert into t(i1, b1, s1, n1, n2, n3) values(1, 1000000000, '123.1', 123.11, 12345678.12, 12345678901234567.123)";

    private String getDDL() {
        switch (getTarget()) {
        case ORACLE:
            return "create table t(i1 integer, b1 number, s1 varchar(32), "
                    + "n1 numeric(4, 1), n2 numeric(10, 2), n3 numeric(20, 3))";
        default:
            return tableDDL;
        }
    }

    @BeforeClass
    public static void log() {
        System.out.println(tableDDL);
        System.out.println(insertStmt);
    }

    @Test
    public void testSQLServerParameters() {
        setTarget(TARGET.SQLSERVER);
        testAll();
    }

    @Test
    public void testPostgresParameters() {
        setTarget(TARGET.POSTGRES);
        setup();
        // testMetadata();
        // testOverflowExecute();
        // testIntegerWithBigDecimal();
        // testStringNumberOperations();
        // testUnions();
        testAggregates();
    }

    @Test
    public void testDb2Parameters() {
        setTarget(TARGET.DB2);
        testAll();
    }

    @Test
    public void testOracleParameters() {
        setTarget(TARGET.ORACLE);
        testAll();
    }

    private void testAll() {
        setup();
        // testMetadata();
        // testOverflowExecute();
        // testStringNumberOperations();
        // testIntegerWithBigDecimal();
        // testUnions();
        testAggregates();
    }

    private void testMetadata() {
        // String tableDDL = "create table t(i1 integer, b1 bigint, s1 varchar(32), "
        //    + "n1 numeric(4, 1), n2 numeric(10, 2), n3 numeric(20, 3));";
        String query = "SELECT i1 + ? as coli1, b1 + ? as colb1, n1 + ? as coln1, n2 + ? as coln2, n3 + ? as coln3 FROM t";
        System.out.println(getTarget() + ":" + query);
        try (Connection c = connect(); PreparedStatement s = c.prepareStatement(query)) {
            ResultSetMetaData metadata = s.getMetaData();
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                System.out.println(String.format("%s : %s(%d,%d)", metadata.getColumnName(i + 1),
                        metadata.getColumnTypeName(i + 1),
                        metadata.getPrecision(i + 1), metadata.getScale(i + 1)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void testOverflowExecute() {
        // String tableDDL = "create table t(i1 integer, b1 bigint, s1 varchar(32), "
        //    + "n1 numeric(4, 1), n2 numeric(10, 2), n3 numeric(20, 3));";
        BigDecimal bi = new BigDecimal("123456.123456");

        String query = "SELECT n1 + ? as coln1, n1 + 123456.123456 actualn1 FROM t";
        System.out.println(getTarget() + ":" + query);
        try (Connection c = connect(); PreparedStatement s = c.prepareStatement(query)) {
            s.setObject(1, bi);
            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                System.out.println("result:" + rs.getString(1) + " vs " + rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void testIntegerWithBigDecimal() {
        // String tableDDL = "create table t(i1 integer, b1 bigint, s1 varchar(32), "
        //    + "n1 numeric(4, 1), n2 numeric(10, 2), n3 numeric(20, 3));";
        BigDecimal bi = new BigDecimal("12.123");

        String query = "SELECT i1 + ? as coln1 FROM t";
        System.out.println(getTarget() + ":" + query);
        try (Connection c = connect(); PreparedStatement s = c.prepareStatement(query)) {
            s.setObject(1, bi);
            ResultSet rs = s.executeQuery();
            if (rs.next()) {
                System.out.println("result:" + rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void testStringNumberOperations() {
        String query = "SELECT i1 + '12345678' as coli1, i1 + s1 as coli1s1, n1 + '123456789' as coln1, n1 + s1 as coln1s1 FROM t";
        System.out.println(getTarget() + ":" + query);
        try (Connection c = connect(); PreparedStatement s = c.prepareStatement(query)) {
            ResultSetMetaData metadata = s.getMetaData();
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                System.out.println(String.format("%s : %s(%d,%d)", metadata.getColumnName(i + 1),
                        metadata.getColumnTypeName(i + 1),
                        metadata.getPrecision(i + 1), metadata.getScale(i + 1)));
            }
            ResultSet rs = s.executeQuery();
            while (rs.next()) {
                System.out.println("{" + rs.getString(1) + "->" + rs.getString(2) + "}, {" + rs.getString(3) + "->"
                        + rs.getString(4) + "}");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void testUnions() {
        String query = "SELECT i1 AS col1 FROM t UNION ALL SELECT s1 AS col1 FROM t";
        System.out.println(getTarget() + ":" + query);
        try (Connection c = connect(); PreparedStatement s = c.prepareStatement(query)) {
            ResultSetMetaData metadata = s.getMetaData();
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                System.out.println(String.format("%s : %s(%d,%d)", metadata.getColumnName(i + 1),
                        metadata.getColumnTypeName(i + 1),
                        metadata.getPrecision(i + 1), metadata.getScale(i + 1)));
            }
            ResultSet rs = s.executeQuery();
            while (rs.next()) {
                System.out.println("{" + rs.getString(1) + "}");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void testAggregates() {
        String query = "SELECT SUM(s1) AS coli1 FROM t";
        System.out.println(getTarget() + ":" + query);
        try (Connection c = connect(); PreparedStatement s = c.prepareStatement(query)) {
            ResultSetMetaData metadata = s.getMetaData();
            for (int i = 0; i < metadata.getColumnCount(); i++) {
                System.out.println(String.format("%s : %s(%d,%d)", metadata.getColumnName(i + 1),
                        metadata.getColumnTypeName(i + 1),
                        metadata.getPrecision(i + 1), metadata.getScale(i + 1)));
            }
            ResultSet rs = s.executeQuery();
            while (rs.next()) {
                System.out.println("{" + rs.getString(1) + "}");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setup() {
        try (Connection c = connect()) {
            try (Statement s = c.createStatement()) {
                s.execute("DROP TABLE t");
            } catch (SQLException e) {
            }

            Statement s = c.createStatement();
            s.execute(getDDL());
            s.execute(insertStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
