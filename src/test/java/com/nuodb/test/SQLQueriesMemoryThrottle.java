package com.nuodb.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.nuodb.tests.MyTest;


public class SQLQueriesMemoryThrottle extends MyTest {
	static volatile boolean keepRunning = true;
	public void startup() throws Exception {

		try (Connection c = openConnection(); Statement s = c.createStatement()) {
			s.execute("drop table if exists t1");
			s.execute("create table t1(i int, s string)");
			s.execute("insert into t1 select rownum(), rownum() from system.fields");
		}
	}
	
	public static void main(String args[]) {
		SQLQueriesMemoryThrottle test = new SQLQueriesMemoryThrottle();
		try {
			test.readConfiguration();
			test.startup();
			test.testConnectionKilled();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testConnectionKilled() throws InterruptedException, ExecutionException {
		ExecutorService service = Executors.newFixedThreadPool(11);
		Future<TestResult> throttleCandidate = service.submit(new BackgroundExecutor(() -> {
			try (Connection c = openConnection();
				 Statement stmt = c.createStatement();
				 ResultSet rs = stmt.executeQuery(longRunningQuery)) {
				while (rs.next()) {
					// consume
				}

			} catch (Exception e) {
				return new TestResult(e);
			}
			return new TestResult();
		}));

		List<Future<TestResult>> survivorQueries = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			survivorQueries.add(service.submit(new BackgroundExecutor(() -> {
				try (Connection c = openConnection()) {
					while (keepRunning) {
						try (Statement stmt = c.createStatement(); ResultSet rs = stmt.executeQuery(simpleQuery)) {
							while (rs.next()) {
								// consume
							}

						} catch (Exception e) {
							return new TestResult(e);
						}
					}
				}
				return new TestResult();
			})));
		}

		TestResult result = throttleCandidate.get();
		assertTrue(result.getException() != null);
		result.getException().printStackTrace();
		keepRunning = false;

		for (Future<TestResult> task : survivorQueries) {
			TestResult r = task.get();
			if(r.getException() != null) {
				System.out.println("UPS!");
			}
		}
		service.shutdown();
	}

	class TestResult {
		private Exception e;

		public TestResult(Exception e) {
			this.e = e;
		}

		public TestResult() {
			this.e = null;
		}

		public Exception getException() {
			return e;
		}

	}

	@FunctionalInterface
	interface TransactionAction {
		TestResult doAction() throws Exception;
	}

	class BackgroundExecutor implements Callable<TestResult> {
		private TransactionAction action;

		public BackgroundExecutor(TransactionAction action) {
			this.action = action;
		}

		@Override
		public TestResult call() throws Exception {
			return action.doAction();
		}

	}

	final String longRunningQuery = "select t1.s from t1, (select rownum() i, concat(rownum(), 'aaa') as s1 from system.fields t1, system.fields t2, system.fields t3, system.fields t4, system.fields t5, system.fields t6, system.fields t7 order by s1) t2 where t1.i = t2.i";
	final String simpleQuery = "select 'aaa' from dual";
}
