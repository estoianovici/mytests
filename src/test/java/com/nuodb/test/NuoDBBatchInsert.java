package com.nuodb.test;

import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class NuoDBBatchInsert extends MyTest {

    @Test
    public void testBatchInsert() {
        // setup();
        Random r = new Random();
        String query = "insert into t1(g, s1, s2, p, o1, o2) values (?, ?, ?, ?, ?, ?)";
        try (Connection c = connect();
             PreparedStatement pinsert = c.prepareStatement(query)) {

            for (int i = 0; i < 1000; i++) {
                int pos = 0;
                pinsert.setLong(++pos, r.nextLong());
                pinsert.setLong(++pos, r.nextLong());
                pinsert.setLong(++pos, r.nextLong());
                pinsert.setLong(++pos, r.nextLong());
                pinsert.setLong(++pos, r.nextLong());
                pinsert.setLong(++pos, r.nextLong());
                pinsert.addBatch();
            }
            pinsert.executeBatch();
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

    }

    private void setup() {
        String[] queries = { "drop schema user cascade",
                "create table t1(g bigint, s1 bigint, s2 bigint, p bigint, o1 bigint, o2 bigint, value string)",
                "create index t1_idx_0 on t1(s1, s2, p, g, o1, o2)",
                "create index t1_idx_1 on t1(g, o2, p, s2, o1, s1)" };
        try (Connection c = openConnection(); Statement s1 = c.createStatement()) {
            runQueries(c, queries);
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
