package com.nuodb.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;
import com.nuodb.tests.MyTest;

public class HoldableGeneratedKeys extends MyTest {

    private void setup() {
        try (Connection conn = connect(); Statement stmt = conn.createStatement()) {
            stmt.execute("drop table t1 if exists");
            stmt.execute("create table t1 (f1 int generated always as identity, f2 string);");
            conn.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() {
        setTarget(TARGET.NUODB);
        setup();
        try (Connection conn = connect()) {
            conn.setHoldability(ResultSet.HOLD_CURSORS_OVER_COMMIT);
            conn.setAutoCommit(true);
            Statement stmt = conn.createStatement();
            stmt.execute("insert into t1 (f2) values ('test'), ('test2');", Statement.RETURN_GENERATED_KEYS);
            ResultSet keys = stmt.getGeneratedKeys();
            assertTrue(keys.next());
            assertEquals(1, keys.getInt(1));
            assertTrue(keys.next());
            assertEquals(2, keys.getInt(1));
            assertFalse(keys.next());
            stmt.close();
        } catch (Throwable t) {
            t.printStackTrace();
            fail("didn't get exception" + t.getMessage());
        }
    }

}
