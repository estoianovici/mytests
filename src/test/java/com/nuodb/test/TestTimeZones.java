package com.nuodb.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Properties;
import java.util.TimeZone;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class TestTimeZones extends MyTest {

    @Test
    public void testGetObject() {
        try (Connection c = connect(); Statement s = c.createStatement()) {
            ResultSet rs = s.executeQuery("SELECT NOW() FROM DUAL");
            if (rs.next()) {
                Object o = rs.getObject(1);
                System.out.println(o.getClass().getName());
            } 
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    @Test
    public void testTimezones() {
        try (Connection c = connect()) {
            runQueries(c, "drop table if exists t",
                    "create table t(id bigint generated always as identity, timestamp_col timestamp, timezone string)");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        Properties properties = new Properties();
        properties.setProperty("user", getUserName());
        properties.setProperty("password", getPassword());
        properties.setProperty("schema", getSchemaName());

        testTimeZone(properties, "EST");
        testTimeZone(properties, "UTC");
    }

    private void testTimeZone(Properties properties, String timezone) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
        String url = String.format("jdbc:com.nuodb://%s:%s/%s?TimeZone=%s", getHostName(), getPort(), getDatabaseName(), timezone);
        try (Connection c = DriverManager.getConnection(url, properties); Statement stmt = c.createStatement()) {
            ResultSet rs = stmt.executeQuery(
                    "select property, value from system.connectionproperties where property = 'timezone'");
            while (rs.next()) {
                System.out.println("Connection timezone is:" + rs.getString(2));
            }

            stmt.execute(
                    "insert into t(timestamp_col, timezone) values(now(), (select property||'='||value from system.connectionproperties where property = 'timezone' limit 1))");
            stmt.execute(
                    "insert into t(timestamp_col, timezone) values('2010-01-01 00:00:00', (select property||'='||value from system.connectionproperties where property = 'timezone' limit 1))");
            rs = stmt.executeQuery("SELECT id, timezone, timestamp_col FROM t");
            while (rs.next()) {
                String line = String.format("%s(%s):{getString= %s, getTimestamp = %s, getTimestampWithTimezone = %s}",
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getTimestamp(3).toString(),
                        rs.getTimestamp(3, cal).toString());
                System.out.println(line);
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}
