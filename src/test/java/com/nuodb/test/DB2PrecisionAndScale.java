package com.nuodb.test;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.junit.Test;

import com.nuodb.tests.MyTest;

public class DB2PrecisionAndScale extends MyTest {

    @Test
    public void test() {
        setTarget(TARGET.NUODB);
        String[] ddl = {
                "CREATE TABLE testable (my_decimal NUMERIC(6,3));",
                "insert into testable values (10.0), (10.000), (10.05), (10.10), (10.123456);"
        };

        try (Connection c = connect(); Statement stmt = c.createStatement()) {
            for (String query : ddl) {
                stmt.execute(query);
            }
            c.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (Connection c = connect(); Statement stmt = c.createStatement()) {
            ResultSet rs = stmt.executeQuery("select my_decimal from testable");
            while (rs.next()) {
                BigDecimal bd = rs.getBigDecimal(1);
                String val = rs.getString(1);
                System.out.println(val +
                        " -- " + rs.getObject(1).getClass().getName() +
                        " -- " + bd.toString() +
                        " -- (" + bd.precision() + ", " + bd.scale() + ")");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
